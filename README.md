# BO-ECLI Parser Engine Italian implementation

The project extends the BOECLI Parser Engine project in order to add support for the Italian language and jurisdiction.

It provides the specific Italian extensions to the vocabularies of authorities, aliases and standard identifiers.

The implementation of services for entity identification, reference recognition and identifier generation are mainly realized with JFlex (http://www.jflex.de).
