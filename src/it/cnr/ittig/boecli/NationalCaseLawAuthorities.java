/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli;

import eu.boecli.common.CaseLawAuthority;

public enum NationalCaseLawAuthorities implements CaseLawAuthority {

	
	/*
	 * Extension for Italian case-law authorities.
	 */


	//Corte Costituzionale
	IT_COST,
	
	//Corte Suprema di Cassazione
	IT_CASS,
	
	//Corte dei Conti
	IT_CONT,
	
	//Consiglio di Stato
	IT_CDS,
	
	//Consiglio di giustizia amministrativa per la Regione Siciliana
	IT_CGARS,
	
	IT_TAR, //Generic TAR
	
	IT_TARAQ, //Abruzzo, L'Aquila
	IT_TARPE, //Abruzzo, Pescara
	IT_TARPZ, //Basilicata, Potenza
	IT_TARCZ, //Calabria, Catanzaro
	IT_TARRC, //Calabria, Reggio Calabria
	IT_TARNA, //Campania, Napoli
	IT_TARSA, //Campania, Salerno
	IT_TARBO, //Emilia-Romagna, Bologna
	IT_TARPR, //Emilia-Romagna, Parma
	IT_TARTS, //Friuli-Venezia Giulia, Trieste
	IT_TARRM, //Lazio, Roma
	IT_TARLT, //Lazio, Latina
	IT_TARGE, //Liguria, Genova
	IT_TARMI, //Lombardia, Milano
	IT_TARBS, //Lombardia, Brescia
	IT_TARAN, //Marche, Ancona
	IT_TARCB, //Molise, Campobasso
	IT_TARTO, //Piemonte, Torino
	IT_TARBA, //Puglia, Bari
	IT_TARLE, //Puglia, Lecce
	IT_TARCA, //Sardegna, Cagliari
	IT_TARPA, //Sicilia, Palermo
	IT_TARCT, //Sicilia, Catania	
	IT_TARFI, //Toscana, Firenze
	IT_TARTN, //Trentino-Alto Adige, Trento
	IT_TARBZ, //Trentino-Alto Adige, Bolzano	
	IT_TARPG, //Umbria, Perugia
	IT_TARAO, //Valle d'Aosta, Aosta	
	IT_TARVE, //Veneto, Venezia
	
	
	IT_CTR, //Commissione Tributaria Regionale
	IT_CTP, //Commissione Tributaria Provinciale
	
	IT_CTPTO, //Commissione Tributaria Provinciale di Torino
	
	
	
	//Corte d'Appello
	IT_CPP,
	
	//Corte di Appello di Torino
	IT_CPPTO,
	
	//Corte di Appello di Napoli
	IT_CPPNA,
	
	
	
	IT_TRB, //Tribunale ordinario

	IT_PTR, //Pretura
	
	
	
	
	
}
