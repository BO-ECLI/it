/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;
import it.cnr.ittig.boecli.NationalIdentifiers;
import it.cnr.ittig.boecli.NationalLegislationAliases;
import it.cnr.ittig.boecli.impl.Config;

public class AliasesGeneration extends IdentifierGenerationService {


	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Alias Generation Service"; }

	@Override
	public String version() { return "0.4"; }
	
	
	private void addNirUrl(LegalIdentifier legalIdentifier) {
		
		legalIdentifier.setUrl("http://www.normattiva.it/uri-res/N2Ls?" + legalIdentifier.getCode());
	}
	
	
	private String getPartitionValue(LegalReference legalReference, String prefix){
		
		String partValue = "";			
		int partStart = legalReference.getPartition().indexOf(prefix);
		if(partStart!=-1){
			int partEnd = legalReference.getPartition().indexOf(";", partStart);
			partValue = legalReference.getPartition().substring(partStart + prefix.length(), partEnd);
			partValue = partValue.replaceAll("-", "");
		}
		
		return partValue;
	}
	
	
	@Override
	protected final Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) throws IllegalArgumentException {

		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();
		
		LegalIdentifier legalIdentifier = null;

		
		//TODO
		// v. http://www.normattiva.it/static/codici.html
		
		
		
		String partition ="";
		
		if( !legalReference.getPartition().equals("")) {
			
			String artValue = getPartitionValue(legalReference, "article-");
			String comValue = getPartitionValue(legalReference, "paragraph-");
			String letValue = getPartitionValue(legalReference, "letter-");
			String numValue = getPartitionValue(legalReference, "item-");
			
			if(!artValue.equals(""))
				if(!partition.equals("")) partition+="-";
				partition+="art"+artValue;
			if(!comValue.equals("")){
				if(!partition.equals("")) partition+="-";
				partition+="com"+comValue;
			}
			if(!letValue.equals("")){
				if(!partition.equals("")) partition+="-";
				partition+="let"+letValue;
			}
			if(!numValue.equals("")){
				if(!partition.equals("")) partition+="-";
				partition+="num"+numValue;
			}
			
			partition= "~"+partition;
		}

		
		String code = "";
		
		switch(NationalLegislationAliases.valueOf(legalReference.getAliasValue())) {
		
			case IT_COST :
				code = "urn:nir:stato:costituzione" + partition;
				break;

			case IT_COD_CIV :
				code = "urn:nir:stato:codice.civile:1942-03-16;262" + partition;
				break;

			case IT_COD_PEN :
				code = "urn:nir:stato:codice.penale:1930-10-19;1398" + partition;
				// non ancora pubblicato su Normattiva
				break;
			
			case IT_COD_PROC_CIV :
				code = "urn:nir:stato:codice.procedura.civile:1940-10-28;1443" + partition;
				break;
			
			case IT_COD_PROC_PEN :
				code = "urn:nir:presidente.repubblica:codice.procedura.penale:1988-09-22;447" + partition;
				break;
		}

		if( !code.equals("")) {
			
			legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(NationalIdentifiers.IT_URNNIR, code);
			
			if(legalIdentifier != null) {
			
				addNirUrl(legalIdentifier);
				legalIdentifier.setConfidence(0.8);
				legalIdentifiers.add(legalIdentifier);
			}
		}
		
		return legalIdentifiers;
	}
}
