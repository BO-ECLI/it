/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;
import it.cnr.ittig.boecli.NationalCaseLawAuthorities;
import it.cnr.ittig.boecli.impl.Config;

public class AuditCourtECLI extends IdentifierGenerationService {

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "ECLI Generation Service for the Italian Audit Court"; }

	@Override
	public String version() { return "0.0"; }

	
	@Override
	protected final Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {
		
		String authority = legalReference.getAuthority();
		
		//if auth is set, the it must be IT_CONT
		if( !authority.equals("") && 
				!authority.equals(NationalCaseLawAuthorities.IT_CONT.toString())) {
			
			return null;
		}

		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();

		
		
		return legalIdentifiers;
	}
	
}
