/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;
import it.cnr.ittig.boecli.NationalIdentifiers;
import it.cnr.ittig.boecli.NationalLegislationTypes;
import it.cnr.ittig.boecli.impl.Config;

public class URNNIRGeneration extends IdentifierGenerationService {

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian URN NIR Generation Service"; }

	@Override
	public String version() { return "0.2"; }

	
	
	
	private String getPartitionValue(LegalReference legalReference, String prefix){
		
		String partValue = "";			
		int partStart = legalReference.getPartition().indexOf(prefix);
		if(partStart!=-1){
			int partEnd = legalReference.getPartition().indexOf(";", partStart);
			partValue = legalReference.getPartition().substring(partStart + prefix.length(), partEnd);
			partValue = partValue.replaceAll("-", "");
		}
		
		return partValue;
	}
	
	
	
	@Override
	protected final Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {
		
		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();
		
		LegalIdentifier legalIdentifier = null;

		
		if( !legalReference.isLegislationReference()) {
			
			return null;
		}
		
		String type = "";
		String auth = "";
		
		double conf = 0.1;
		
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.LAW.toString())) { type = "legge"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.DECREE.toString())) { type = "decreto"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.DECREE_LAW.toString())) { type = "decreto.legge"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.LEGISLATIVE_DECREE.toString())) { type = "decreto.legislativo"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.CONSTITUTIONAL_LAW.toString())) { type = "legge.costituzionale"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.ROYAL_DECREE.toString())) { type = "regio.decreto"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.ROYAL_LEGISLATIVE_DECREE.toString())) { type = "regio.decreto.legislativo"; auth = "stato"; conf = 0.9; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.ORDER.toString())) { type = "ordinanza"; conf = 0.4; }
		
		if(legalReference.getType().equalsIgnoreCase(NationalLegislationTypes.IT_ORD_MIN.toString())) { type = "ordinanza"; auth = "ministero."; conf = 0.6; }
		if(legalReference.getType().equalsIgnoreCase(NationalLegislationTypes.IT_DEC_MIN.toString())) { type = "decreto"; auth = "ministero."; conf = 0.6; }
		if(legalReference.getType().equalsIgnoreCase(NationalLegislationTypes.IT_REG_MIN.toString())) { type = "regolamento"; auth = "ministero."; conf = 0.6; }
		
		
		if(!type.equalsIgnoreCase("")){

			String number = legalReference.getNumber();

			String date = legalReference.getYear();
			if( !legalReference.getDate().equals("")) {

				date = legalReference.getDate();

				if(legalReference.getDate().length() < 8) {

					if( !legalReference.getYear().equals("")) {

						date = legalReference.getYear() + "-" + date;
					}
				}
			}

			String partition ="";
			
			if( !legalReference.getPartition().equals("")) {
				
				String artValue = getPartitionValue(legalReference, "article-");
				String comValue = getPartitionValue(legalReference, "paragraph-");
				String letValue = getPartitionValue(legalReference, "letter-");
				String numValue = getPartitionValue(legalReference, "item-");
				
				if(!artValue.equals(""))
					if(!partition.equals("")) partition+="-";
					partition+="art"+artValue;
				if(!comValue.equals("")){
					if(!partition.equals("")) partition+="-";
					partition+="com"+comValue;
				}
				if(!letValue.equals("")){
					if(!partition.equals("")) partition+="-";
					partition+="let"+letValue;
				}
				if(!numValue.equals("")){
					if(!partition.equals("")) partition+="-";
					partition+="num"+numValue;
				}
				
				partition= "~"+partition;
			}
			


			String urnNir = "urn:nir:" + auth + ":" + type + ":" + date + ";" + number + partition;

			String url = "http://www.normattiva.it/uri-res/N2Ls?" + urnNir;

			legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(NationalIdentifiers.IT_URNNIR, urnNir);

			if(legalIdentifier != null) {

				legalIdentifier.setUrl(url);
				legalIdentifier.setConfidence(conf);
				legalIdentifiers.add(legalIdentifier);
			}

		}
		return legalIdentifiers;
	}
	
}
