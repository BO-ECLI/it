/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CommonLegalTypes;
import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.NationalCaseLawAuthorities;
import it.cnr.ittig.boecli.NationalLegislationAliases;
import it.cnr.ittig.boecli.impl.Config;

%%
%class AbbreviationsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Abbreviations identification"; }

	@Override
	public String version() { return "0.7"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public AbbreviationsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private int offset = 0;
	private int length = 0;
	
%} 

BE_ANN = {BE_AUTH}|{BE_TYPE}|{BE_NUMBERYEAR}|{BE_NUMBER}|{BE_CASENUMBER}|{BE_ALTNUMBER}|{BE_DATE}|{BE_SUBJECT}|{BE_MISC}

BE_PARTS = {BE_PART}|{BE_ARTICLE}|{BE_PARAGRAPH}|{BE_LETTER}|{BE_ITEM}



TRIB = (trib\.?)|(t\.?)
LAV = (lav\.?)|(l\.?)
FER = (fer\.?)|(f\.?)
UU = (uu\.?)

FIRST = (prima)|(i)|(1[\^°ºªa])
SECOND = (seconda)|(ii)|(2[\^°ºªa])
THIRD = (terza)|(iii)|(3[\^°ºªa])
FOURTH = (quarta)|(iv)|(4[\^°ºªa])
FIFTH = (quinta)|(v)|(5[\^°ºªa])
SIXTH = (sesta)|(vi)|(6[\^°ºªa])


BE_ANN_LEG_COST = {BE_ARTICLE}|{BE_PARAGRAPH}|{BE_LETTER}|{BE_ITEM}
BE_ANN_CASE_COST = {BE_TYPE}|{BE_NUMBERYEAR}|{BE_NUMBER}|{BE_DATE}

COST = (cost\.?)

CONS = (consiglio)|(cons\.?)


/* European legislation types of document */

DIR = (dir\.?)
DIR_TREATY = {DIR}{SE}?{TREATY}
REG = (reg\.?)
REG_TREATY = {REG}{SE}?{TREATY}
DEC = (dec\.?)
DEC_TREATY = {DEC}{SE}?{TREATY}
REC = (racc\.?)
REC_TREATY = {REC}{SE}?{TREATY}


/* Abbraviations for codes */

/* Codice civile */
CODE = (codice)|(cod\.?)|(c\.?)
CIVIL = (civile)|(civ\.?)|(c\.?)
CIVIL_CODE = {CODE}{SDHE}?{CIVIL}

/* Codice penale */
CRIM = (penale)|(pen\.?)|(p\.?)
CRIM_CODE = {CODE}{SDHE}?{CRIM}

/* Codice di procedura civile */
PROC = (procedura)|(proc\.?)|(p\.?)
CIVIL_PROC_CODE = {CODE}{SE}?{OF}?{SE}?{PROC}{SE}?{CIVIL}

/* Codice di procedura penale */
CRIM_PROC_CODE = {CODE}{SE}?{OF}?{SE}?{PROC}{SE}?{CRIM}

CODES = {CIVIL_CODE}|{CRIM_CODE}|{CIVIL_PROC_CODE}|{CRIM_PROC_CODE}


LAW = (l\.?)
DECREE = (d\.?)|(dec\.?)

%x BE legCost caseCost codes   

%%

/* l. n. 516 del 1982... */


({NAD}{LAW})/({SE}?({BE_NUMBER}|{BE_NUMBERYEAR}|{BE_DATE}))	{ 
																	addText(yytext().substring(0,1)); 
																	addType(CommonLegislationTypes.LAW, yytext().substring(1,yylength())); 
																}

({NAD}{DECREE})/({SE}?({BE_NUMBER}|{BE_NUMBERYEAR}|{BE_DATE}))	{ 
																	addText(yytext().substring(0,1)); 
																	addType(CommonLegalTypes.DECREE, yytext().substring(1,yylength())); 
																}

({NAD}{TRIB})/({SCDHE}?{BE_ANN})		{ 
											addText(yytext().substring(0,1)); 
											addSection("T", yytext().substring(1,yylength())); 
										}

({NAD}{LAV})/({SCDHE}?{BE_ANN})			{ 
											addText(yytext().substring(0,1)); 
											addSection("L", yytext().substring(1,yylength())); 
										}

({NAD}{FER})/({SCDHE}?{BE_ANN})			{ 
											addText(yytext().substring(0,1)); 
											addSection("F", yytext().substring(1,yylength())); 
										}

({NAD}{UU})/({SCDHE}?{BE_ANN})			{ 
											addText(yytext().substring(0,1)); 
											addSection("U", yytext().substring(1,yylength())); 
										}

({NAD}{FIRST})/({SCDHE}?{BE_ANN})		{ addText(yytext().substring(0,1));	addSection("1", yytext().substring(1,yylength())); }
({NAD}{SECOND})/({SCDHE}?{BE_ANN})		{ addText(yytext().substring(0,1));	addSection("2", yytext().substring(1,yylength())); }
({NAD}{THIRD})/({SCDHE}?{BE_ANN})		{ addText(yytext().substring(0,1));	addSection("3", yytext().substring(1,yylength())); }
({NAD}{FOURTH})/({SCDHE}?{BE_ANN})		{ addText(yytext().substring(0,1));	addSection("4", yytext().substring(1,yylength())); }
({NAD}{FIFTH})/({SCDHE}?{BE_ANN})		{ addText(yytext().substring(0,1));	addSection("5", yytext().substring(1,yylength())); }
({NAD}{SIXTH})/({SCDHE}?{BE_ANN})		{ addText(yytext().substring(0,1));	addSection("6", yytext().substring(1,yylength())); }



{NAD}{DIR_TREATY}{NAD}			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.DIRECTIVE, yytext().substring(1,yylength())); }
{NAD}{REG_TREATY}{NAD}			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.REGULATION, yytext().substring(1,yylength())); }
{NAD}{DEC_TREATY}{NAD}			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.DECISION, yytext().substring(1,yylength())); }
{NAD}{REC_TREATY}{NAD}			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.RECOMMENDATION, yytext().substring(1,yylength())); }

{NAD}{DIR}/({SE}?({BE_NUMBER}|{BE_NUMBERYEAR}|{BE_DATE}))			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.DIRECTIVE, yytext()); }
{NAD}{REG}/({SE}?({BE_NUMBER}|{BE_NUMBERYEAR}|{BE_DATE}))			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.REGULATION, yytext()); }
{NAD}{REC}/({SE}?({BE_NUMBER}|{BE_NUMBERYEAR}|{BE_DATE}))			{ addText(yytext().substring(0,1)); addType(CommonLegislationTypes.RECOMMENDATION, yytext()); }



({BE_ANN_LEG_COST}{BE_SEP}?{COST}{NAD})|
({NAD}{COST}{BE_SEP}?{BE_ANN_LEG_COST})			{ offset = 0; length = yylength(); yypushback(length); yybegin(legCost); }

<legCost> {

	{COST}		{ addAlias(NationalLegislationAliases.IT_COST, yytext()); offset += yylength(); }
	
	[^]		{
				offset++;
				
				if( offset >= length ) {

					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}
 


({NAD}{COST}{BE_SEP}?{BE_ANN_CASE_COST})		{ offset = 0; length = yylength(); yypushback(length); yybegin(caseCost); }

<caseCost> {

	{COST}		{ addAuthority(NationalCaseLawAuthorities.IT_COST, yytext()); offset += yylength(); }
	
	[^]		{
				offset++;
				
				if( offset >= length ) {

					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}
 

({BE_PARTS}{BE_SEP}?{CODES}{NAD})|
({NADD}{CODES}{BE_SEP}?{BE_PARTS})			{ offset = 0; length = yylength(); yypushback(length); yybegin(codes); }

<codes> {

	{CIVIL_CODE}				{ addAlias(NationalLegislationAliases.IT_COD_CIV, yytext()); offset += yylength(); }
		
	{CRIM_CODE}					{ addAlias(NationalLegislationAliases.IT_COD_PEN, yytext()); offset += yylength(); }
		
	{CIVIL_PROC_CODE}			{ addAlias(NationalLegislationAliases.IT_COD_PROC_CIV, yytext()); offset += yylength(); }
		
	{CRIM_PROC_CODE}			{ addAlias(NationalLegislationAliases.IT_COD_PROC_PEN, yytext()); offset += yylength(); }

	[^]		{
				offset++;
				
				if( offset >= length ) {

					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{ addText(yytext()); }


