/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Alias;
import eu.boecli.common.CommonLegislationAliases;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.NationalLegislationAliases;
import it.cnr.ittig.boecli.impl.Config;

%%
%class AliasesIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in



%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Alias identification"; }

	@Override
	public String version() { return "0.4"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public AliasesIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(Alias alias) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addAlias(alias, text.substring(1, text.length()-1));
		
		yypushback(1);
	
	}


%} 


/* Ambiguous abbreviations (like "cost." or "c.c.") are covered in the "AbbreviationsIdentification" service */


 
/* Costituzione */
CONSTITUTION = ([C]ostituzione)




/* Codice civile */
CODE = (codice)|(cod\.?)
CIVIL = (civile)|(civ\.?)
CIVIL_CODE = {CODE}{SDHE}?{CIVIL}

/* Codice penale */
CRIM = (penale)|(pen\.?)
CRIM_CODE = {CODE}{SDHE}?{CRIM}

/* Codice di procedura civile */
PROC = (procedura)|(proc\.?)
CIVIL_PROC_CODE = {CODE}{SE}?{OF}?{SE}?{PROC}{SE}?{CIVIL}

/* Codice di procedura penale */
CRIM_PROC_CODE = {CODE}{SE}?{OF}?{SE}?{PROC}{SE}?{CRIM}




/* Trattato sull'Unione Europea (TUE) */
TREATY = (trattato)|(tr\.?)
EUROPEAN = (europea)|(eu\.?)|(e\.?)
UNION = (unione)|(un\.?)|(u\.?)
TEU = {TREATY}{SE}?({OF}|{ON})?{SE}?{UNION}{SE}?{EUROPEAN}
TEU_ABBR_UPPER = ([T]\.?[U]\.?[E]\.?)
TEU_ABBR = (t\.?u\.?e\.?)
TEU_BRACKETS = [\(]{TEU_ABBR}[\)]
TEU_SUFFIX = {TEU_BRACKETS}|{TEU_ABBR}


/* Trattato sul funzionamento dell'Unione europea (TFUE) */
FUNCT = (funzionamento)|(funz\.?)
TFEU = {TREATY}{SE}?({OF}|{ON})?{SE}?{FUNCT}{SE}?({OF}|{ON})?{SE}?{UNION}{SE}?{EUROPEAN}
TFEU_ABBR = (t\.?f\.?u\.?e\.?)
TFEU_BRACKETS = [\(](t\.?f\.?u\.?e\.?)[\)]
TFEU_SUFFIX = {TFEU_BRACKETS}|{TFEU_ABBR}



%x BE   

%%

{NAD}{CONSTITUTION}{NAD}			{ save(NationalLegislationAliases.IT_COST); }



{NAD}{CIVIL_CODE}{NAD}				{ save(NationalLegislationAliases.IT_COD_CIV); }
{NAD}{CRIM_CODE}{NAD}				{ save(NationalLegislationAliases.IT_COD_PEN); }
{NAD}{CIVIL_PROC_CODE}{NAD}			{ save(NationalLegislationAliases.IT_COD_PROC_CIV); }
{NAD}{CRIM_PROC_CODE}{NAD}			{ save(NationalLegislationAliases.IT_COD_PROC_PEN); }



{NAD}{TEU}{NAD}						{ save(CommonLegislationAliases.TREATY_EU); }
{NADD}{TEU_ABBR_UPPER}{NAD}			{ save(CommonLegislationAliases.TREATY_EU); }
{NAD}{TEU_BRACKETS}{NAD}			{ save(CommonLegislationAliases.TREATY_EU); }
{NAD}{TEU}{SDHE}?{TEU_SUFFIX}{NAD}	{ save(CommonLegislationAliases.TREATY_EU); }



{NAD}{TFEU}{NAD}					{ save(CommonLegislationAliases.TREATY_FUNCTIONING_EU); }
{NADD}{TFEU_ABBR}{NAD}				{ save(CommonLegislationAliases.TREATY_FUNCTIONING_EU); }
{NAD}{TEU}{SDHE}?{TFEU_SUFFIX}{NAD}	{ save(CommonLegislationAliases.TREATY_FUNCTIONING_EU); }




{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{ addText(yytext()); }


