/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CommonLegislationAuthorities;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.common.LegislationAuthority;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.NationalLegislationAuthorities;
import it.cnr.ittig.boecli.impl.Config;

%%
%class AuthoritiesIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

    @Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian authorities identification"; }

	@Override
	public String version() { return "0.2"; }


	/* An empty default constructor is required to comply with BOECLIService */
	
	public AuthoritiesIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(LegislationAuthority authority) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addAuthority(authority, text.substring(1, text.length()-1));
		
		yypushback(1);
	}


%} 

/* Identification of legal authorities in the Italian language */ 



JUDGMENT = (sentenza)|(sentenze)|(sent\.?)|(decisione)|(pronuncia)


/* presidente del consiglio dei ministri */
PRESIDENT = (presidente)|(pres\.?)
COUNCIL = (consiglio)|(cons\.?)
MINISTRY = (ministri)|(min\.?)
PresCM = {PRESIDENT}{SE}?{OF}?{SE}?{COUNCIL}{SE}?{OF}?{SE}?{MINISTRY}

/* presidente della repubblica */
REPUBLIC = (repubblica)|(rep\.?)
PresRep = {PRESIDENT}{SE}?{OF}?{SE}?{REPUBLIC}


/* Unione europea */

UNION = (unione)|(un\.?)|(u\.?)
EUROPEAN = (europe.)|(eur\.?)|(eu\.?)|(e\.?)
ofUE = ({OF}{SE}?)?{UNION}{SE}?({OF}{SE}?)?{EUROPEAN}

ConsUE = {COUNCIL}{SE}?{ofUE}

PARLIAMENT = (parlamento)|(parl\.?)
ParlUE = {PARLIAMENT}{SE}?{ofUE}

COMMISSION = (commissione)|(comm\.?)
CommUE = {COMMISSION}{SE}?{ofUE}

ParlConsUE = {PARLIAMENT}{SE}?{ofUE}{SE}?{AND}{SE}?({OF}{SE}?)?{COUNCIL}{SE}?{ofUE}
ParlEuCons = {PARLIAMENT}{SE}?{EUROPEAN}{SE}?{AND}{SE}?({OF}{SE}?)?{COUNCIL}


/*

del Parlamento europeo e del Consiglio


capo provvisorio dello stato
capo del governo
Presidente della Camera dei Deputati
Presidente del Senato della Repubblica
Ufficio di presidenza
(presidente della)? | (ufficio di presidenza del)?/consiglio/giunta regionale/provinciale/comunale

*/



%x BE  

%%


{NAD}{PresCM}{NAD}				{ save(NationalLegislationAuthorities.IT_PRES_CONS_MIN); }

{NAD}{PresRep}{NAD}				{ save(NationalLegislationAuthorities.IT_PRES_REP); }


{NAD}{ConsUE}{NAD}				{ save(CommonLegislationAuthorities.EU_COUNCIL); }

{NAD}{CommUE}{NAD}				{ save(CommonLegislationAuthorities.EU_COMMISSION); }

{NAD}{ParlUE}{NAD}				{ save(CommonLegislationAuthorities.EU_PARLIAMENT); }

{NAD}{ParlConsUE}{NAD}			{ save(CommonLegislationAuthorities.EU_PARLIAMENT_COUNCIL); }

{NAD}{ParlEuCons}{NAD}			{ save(CommonLegislationAuthorities.EU_PARLIAMENT_COUNCIL); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]    				{ addText(yytext()); }


