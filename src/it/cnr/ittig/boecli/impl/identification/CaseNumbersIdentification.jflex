/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class CaseNumbersIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Case Numbers and Years"; }

	@Override
	public String version() { return "0.8"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public CaseNumbersIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private int offset = 0;	
	private int length = 0;
	private StringBuilder text = new StringBuilder();
	
	private ArrayList<String> numbers = new ArrayList<String>();
	private ArrayList<String> years = new ArrayList<String>();
	private ArrayList<String> prefixes = new ArrayList<String>();
	
	private static final Pattern digits = Pattern.compile("\\d+");
	
	private void start() {
	
		addText(yytext().substring(0,1));
		numbers = new ArrayList<String>();
		years = new ArrayList<String>();
		prefixes = new ArrayList<String>();
		text = new StringBuilder();
		offset = 1; // avoid starting NDA char
		length = yylength() - 1; // avoid final ND char
		yypushback(length);
		
		yybegin(read);
	}
	
	private void save(String prefix) {
	
		offset += yylength();
		text.append(yytext());

		String number = "";
		String year = "";

		Matcher matcher = digits.matcher(yytext());
		
		while(matcher.find()) {
		
			if(number.equals("")) {
			
				number = yytext().substring(matcher.start(), matcher.end());
			
			} else {
			
				year = yytext().substring(matcher.start(), matcher.end());
				break;
			}		
		}
		
		numbers.add(number);
		years.add(year);
		prefixes.add(prefix);
	}
	
%} 

PREFIX_NUMBER = (numer[oi])|(num\.?)|(n\.?)|(nn\.?)|(no\.)|(n°)

RC = ({PREFIX_NUMBER}{SE}?)?(r\.?{SE}?c\.?)

RG = ({PREFIX_NUMBER}{SE}?)?(r\.?{SE}?g\.?)

CASE = (caus[ae])|(process[oi])|(procediment[oi])|(proc\.)|(ricors[oi])|(ric\.?)|(appl\.)|(cas[oi])|(ruolo)|{RC}|{RG}

PREFIX_CASE = ({ISSUED}{SE}?({BY}{SE})?)?({IN}{SE})?{CASE}

NUMBERYEAR_SEP = {YEAR_PREFIX}|{ANY_SEP}
NUMBER_YEAR = {DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}

CASE_C1 = (c){SE}?{DH}{SE}?{NUMBER_YEAR}
CASE_C2 = (c){SE}?{NUMBER_YEAR}
CASE_C3 = (c){SE}?{SLASHES}{SE}?{NUMBER_YEAR}
CASE_C = {CASE_C1}|{CASE_C2}|{CASE_C3}

CASE_T1 = (t){SE}?{DH}{SE}?{NUMBER_YEAR}
CASE_T2 = (t){SE}?{NUMBER_YEAR}
CASE_T3 = (t){SE}?{SLASHES}{SE}?{NUMBER_YEAR}
CASE_T = {CASE_T1}|{CASE_T2}|{CASE_T3}


JOINT = (riunit.)

MULTIPLE_CASES_C1 = {CASE_C}({SCDHE}{CASE_C})+
MULTIPLE_CASES_C2 = {CASE_C}{SE}?{AND}{SE}?{CASE_C}
MULTIPLE_CASES_C3 = {CASE_C}({SCDHE}{CASE_C})+{SE}?{AND}{SE}?{CASE_C}
MULTIPLE_CASES_C = {MULTIPLE_CASES_C1}|{MULTIPLE_CASES_C2}|{MULTIPLE_CASES_C3}

MULTIPLE_CASES_T1 = {CASE_T}({SCDHE}{CASE_T})+
MULTIPLE_CASES_T2 = {CASE_T}{SE}?{AND}{SE}?{CASE_T}
MULTIPLE_CASES_T3 = {CASE_T}({SCDHE}{CASE_T})+{SE}?{AND}{SE}?{CASE_T}
MULTIPLE_CASES_T = {MULTIPLE_CASES_T1}|{MULTIPLE_CASES_T2}|{MULTIPLE_CASES_T3}

MULTIPLE_CASES_1 = {NUMBER_YEAR}({SCDHE}{NUMBER_YEAR})+
MULTIPLE_CASES_2 = {NUMBER_YEAR}{SE}?{AND}{SE}?{NUMBER_YEAR}
MULTIPLE_CASES_3 = {NUMBER_YEAR}({SCDHE}{NUMBER_YEAR})+{SE}?{AND}{SE}?{NUMBER_YEAR}
MULTIPLE_CASES = {MULTIPLE_CASES_1}|{MULTIPLE_CASES_2}|{MULTIPLE_CASES_3}


JOINT_CASES_C = ({IN}{SE})?({CASE}{SE}?)?({JOINT}{SE}?)?({PREFIX_NUMBER}{SE}?)?{MULTIPLE_CASES_C}
JOINT_CASES_T = ({IN}{SE})?({CASE}{SE}?)?({JOINT}{SE}?)?({PREFIX_NUMBER}{SE}?)?{MULTIPLE_CASES_T}
JOINT_CASES = ({IN}{SE})?({CASE}{SE}?)({JOINT}{SE}?)?({PREFIX_NUMBER}{SE}?)?{MULTIPLE_CASES}


%x BE read   

%%


{NAD}({PREFIX_CASE}{SE}?)?({PREFIX_NUMBER}{SE}?)?{CASE_C}{ND} { start(); }

{NAD}({PREFIX_CASE}{SE}?)?({PREFIX_NUMBER}{SE}?)?{CASE_T}{ND} { start(); }

{NAD}{PREFIX_CASE}{SE}?({PREFIX_NUMBER}{SE}?)?{NUMBER_YEAR}{ND} { start(); }


({NAD}{JOINT_CASES_C}{ND})|
({NAD}{JOINT_CASES_T}{ND})|
({NAD}{JOINT_CASES}{ND})	{ start(); }
							
<read> {

	{CASE_C}		{ save("C"); }
	{CASE_T}		{ save("T"); }
	({PREFIX_CASE}{SE}?)?({PREFIX_NUMBER}{SE}?)?{NUMBER_YEAR}	{ save(""); }
	
	[^]	{
			offset++;
			
			if( offset >= length ) {
				
				addJointCaseNumbers(prefixes, numbers, years, text.toString());
				
				if(offset > length) yypushback(1);
				
				yybegin(YYINITIAL);
			}
			
			text.append(yytext());
		}
}
							
							
{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]					{ addText(yytext()); }


