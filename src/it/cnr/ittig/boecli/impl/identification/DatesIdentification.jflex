/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class DatesIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

 	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Dates"; }

	@Override
	public String version() { return "0.5"; }
	
		

	/* An empty default constructor is required to comply with BOECLIService */
	
	public DatesIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}	
	
	private static final Pattern digits = Pattern.compile("\\d+");
	
	private String text = "";
	private int offset = 0;
	private int length = 0;
	
	private String day = "";
	private String month = "";
	private String year = "";
	
	private String readNumber() {
	
		Matcher matcher = digits.matcher(yytext());
		String number = "";
		
		while(matcher.find()) {
			
			number = yytext().substring(matcher.start(), matcher.end());			
			break;
		}
		
		return number;
	}
		
%} 

/**********/
/* Months */
/**********/

JAN = (gennaio)|(gen\.?)
FEB = (febbraio)|(feb\.?)
MAR = (marzo)|(mar\.?)
APR = (aprile)|(apr\.?)
MAY = (maggio)|(mag\.?)
JUN = (giugno)|(giu\.?)
JUL = (luglio)|(lug\.?)
AUG = (agosto)|(ago\.?)
SEP = (settembre)|(sett?\.?)
OCT = (ottobre)|(ott\.?)
NOV = (novembre)|(nov\.?)
DEC = (dicembre)|(dic\.?)


MONTH_NAME = {JAN}|{FEB}|{MAR}|{APR}|{MAY}|{JUN}|{JUL}|{AUG}|{SEP}|{OCT}|{NOV}|{DEC}

MONTH_NUMBER = ([0]?[1-9])|([1][0-2])

MONTH = {MONTH_NAME}|{MONTH_NUMBER}


/********/
/* Days */
/********/

FIRST = [1][\^°ºo]

DAY = ([0]?[1-9])|([1-2][0-9])|([3][0-1])|{FIRST}


/************/
/* Prefixes */
/************/

PREFIX1 = (giorno)|(data)
PREFIX2 = {ISSUED}{SE}?({DET}|{OF}|{IN})?{SE}?{PREFIX1}
/* PREFIX = {PREFIX1}|{PREFIX2} */
PREFIX = {ISSUED}{SE}?({DET}|{OF}|{IN})?{SE}?{PREFIX1}?


/**************/
/* Separators */
/**************/

DATE_SEP = ({SE}|{SLASH_SEP}|{BACKSLASH_SEP}|{DOT_SEP}|{DASH_SEP})+


/*******************************/
/* Patterns for complete dates */
/*******************************/

Date0 = {DAY}{SE}?{MONTH_NAME}{SE}?{YEAR}

Date1 = {DAY}{SE}{MONTH}{SE}{YEAR}
Date2 = {DAY}{SE}?{SLASH_SEP}{SE}?{MONTH}{SE}?{SLASH_SEP}{SE}?{YEAR}
Date3 = {DAY}{SE}?{BACKSLASH_SEP}{SE}?{MONTH}{SE}?{BACKSLASH_SEP}{SE}?{YEAR}
Date4 = {DAY}{SE}?{DOT_SEP}{SE}?{MONTH}{SE}?{DOT_SEP}{SE}?{YEAR}
Date5 = {DAY}{SE}?{DASH_SEP}{SE}?{MONTH}{SE}?{DASH_SEP}{SE}?{YEAR}



COMPLETE = {Date0}|{Date1}|{Date2}|{Date3}|{Date4}|{Date5}



%x BE completeDate 

%%


({NAD}{PREFIX}{SE}?{COMPLETE}{ND})|
({ND}{COMPLETE}{ND})
										{
											day = "";
											month = "";
											year = "";
											addText(yytext().substring(0,1));
											offset = 1;
											length = yylength();
											text = yytext().substring(1,length-1);
											yypushback(length-1);
											yybegin(completeDate);
										}

<completeDate>	{

	{DAY}/({DATE_SEP}?{MONTH_NAME}{DATE_SEP}?{YEAR})	{ day = readNumber(); offset += yylength(); }

	{DAY}/({DATE_SEP}{MONTH}{DATE_SEP}{YEAR})			{ day = readNumber(); offset += yylength(); }
	
	{MONTH_NUMBER}/({DATE_SEP}{YEAR})				{ month = readNumber(); offset += yylength(); }
																					
	{JAN}/({DATE_SEP}?{YEAR})						{ month = "1"; offset += yylength(); }
	{FEB}/({DATE_SEP}?{YEAR})						{ month = "2"; offset += yylength(); }
	{MAR}/({DATE_SEP}?{YEAR})						{ month = "3"; offset += yylength(); }
	{APR}/({DATE_SEP}?{YEAR})						{ month = "4"; offset += yylength(); }
	{MAY}/({DATE_SEP}?{YEAR})						{ month = "5"; offset += yylength(); }
	{JUN}/({DATE_SEP}?{YEAR})						{ month = "6"; offset += yylength(); }
	{JUL}/({DATE_SEP}?{YEAR})						{ month = "7"; offset += yylength(); }
	{AUG}/({DATE_SEP}?{YEAR})						{ month = "8"; offset += yylength(); }
	{SEP}/({DATE_SEP}?{YEAR})						{ month = "9"; offset += yylength(); }
	{OCT}/({DATE_SEP}?{YEAR})						{ month = "10"; offset += yylength(); }
	{NOV}/({DATE_SEP}?{YEAR})						{ month = "11"; offset += yylength(); }
	{DEC}/({DATE_SEP}?{YEAR})						{ month = "12"; offset += yylength(); }
																					
	{YEAR}											{ year = readNumber(); offset += yylength(); }

	[^]			{
					offset++;
					
					if( offset >= length ) {

						addDate(day, month, year, text);						
						
						yypushback(1);
						yybegin(YYINITIAL);
					}
				}
}



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]   				{ addText(yytext()); }


