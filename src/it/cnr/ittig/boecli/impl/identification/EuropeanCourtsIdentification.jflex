/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CaseLawAuthority;
import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;


%%
%class EuropeanCourtsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in


%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "European CaseLaw authorities"; }

	@Override
	public String version() { return "0.3"; }
		

	/* An empty default constructor is required to comply with BOECLIService */
	
	public EuropeanCourtsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(CaseLawAuthority authority) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addAuthority(authority, text.substring(1, text.length()-1));
		
		yypushback(1);
	}

%} 




/* Corte di giustizia unione europea */

CGUE = (cgue)
CGCE = (cgce)
COURT = (corte)|(c\.)
JUSTICE = (giustizia)|(giust\.)
UNION = (unione)|(un\.)
COMMUNITY = (comunit.)
EUROPEAN = (europe.)|(eur\.)|(eu\.)

CJEU_1 = {COURT}{SE}?{OF}?{SE}?{JUSTICE} /* Corte di giustizia */
CJEU_2 = {CJEU_1}{SE}?{EUROPEAN} /* Corte di giustizia europea */
CJEU_3 = {CJEU_1}{SE}?{OF}?{SE}?{UNION}({SE}?{EUROPEAN})? /* Corte di giustizia dell'unione europea */
CJEU_4 = {CJEU_1}{SE}?{OF}?{SE}?{COMMUNITY}{SE}?{EUROPEAN} /* Corte di giustizia della comunità europea */
CJEU_5 = {CJEU_1}{SE}?{OF}?{SE}?(u\.?e\.?) /* Corte di giustizia dell'U.E. */
CJEU_6 = {CJEU_1}{SE}?{OF}?{SE}?(c\.?e\.?) /* Corte di giustizia della C.E. */
CJEU = {CJEU_1}|{CJEU_2}|{CJEU_3}|{CJEU_4}|{CJEU_5}|{CJEU_6}


/* Corte europea sui diritti dell'uomo */

CEDU = (cedu)|({COURT}{SE}?(edu))
RIGHTS = (diritti)|(dir\.)|(d\.)
HUMAN = (uomo)|(u\.)
ECHR = {COURT}{SE}?{EUROPEAN}({SE}?({OF}|{ON}))?{SE}?{RIGHTS}({SE}?{OF})?{SE}?{HUMAN}


/* Ufficio Brevetti Europeo (EPO) */
OFFICE = (ufficio)
PATENT = (brevetti)
EPO_ABBR = ([E][P][O])
EPO = {EPO_ABBR}|({OFFICE}{SE}?{PATENT}{SE}?{EUROPEAN}({SDHE}?\(?{EPO_ABBR}\)?)?) 


%x BE   

%%


{NAD}{CGUE}{NAD}		{ save(CommonCaseLawAuthorities.CJEU); }
{NAD}{CGCE}{NAD}		{ save(CommonCaseLawAuthorities.CJEU); }
{NAD}{CJEU}{NAD}		{ save(CommonCaseLawAuthorities.CJEU); }

{NAD}{CEDU}{NAD}		{ save(CommonCaseLawAuthorities.ECHR); }
{NAD}{ECHR}{NAD}		{ save(CommonCaseLawAuthorities.ECHR); }

{NAD}{EPO}{NAD}			{ save(CommonCaseLawAuthorities.EPO); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]    				{ 
						addText(yytext());
					}


