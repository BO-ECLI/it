/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class EuropeanNumbersIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "European legislation numbers"; }

	@Override
	public String version() { return "0.2"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public EuropeanNumbersIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static final Pattern digits = Pattern.compile("\\d+");
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	
	private void save(boolean yearFirst) {
		
		String text = yytext();
		
		addText(text.substring(0,1));
		
		Matcher matcher = digits.matcher(text);
		
		matcher.find();
		
		String first = text.substring(matcher.start(), matcher.end());
		
		matcher.find();
		
		String second = text.substring(matcher.start(), matcher.end());		
		
		if(yearFirst) {
		
			addNumber(second, first, text.substring(1, text.length()-1));
		
		} else {
		
			addNumber(first, second, text.substring(1, text.length()-1));
		}
		
		yypushback(1);
	}
	
%} 
	


PREFIX_SINGLE = (numero)|(num\.?)|(nr\.?)|(n\.?)|(no\.?)|(n°)
PREFIX_MULTIPLE = (numeri)|(num\.?)|(nr\.?)|(n\.?)|(nn\.?)

NUMBER_PREFIX = {PREFIX_SINGLE}|{PREFIX_MULTIPLE}

NUM_SEP = {SE}?{SLASHES}{SE}?

YEAR_NUMBER = {YEAR}{NUM_SEP}{DD}({NUM_SEP}|{SE}){TREATY}

NUMBER_YEAR = {DD}{NUM_SEP}{YEAR}({NUM_SEP}|{SE}){TREATY}



%x BE  

%%


{NAD}({NUMBER_PREFIX}{SE}?)?{YEAR_NUMBER}{NAD}	{ save(true); }

{NAD}({NUMBER_PREFIX}{SE}?)?{NUMBER_YEAR}{NAD}	{ save(false); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]					{ addText(yytext()); }


