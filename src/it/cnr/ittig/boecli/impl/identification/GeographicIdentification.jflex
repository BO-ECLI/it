/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CommonCaseLawTypes;
import eu.boecli.common.CommonLegalTypes;
import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.common.Type;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class GeographicIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in
%include ../GeographicMacros.in

%{
	
	/* Custom java code */

    @Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Geographic entities for Italian language"; }

	@Override
	public String version() { return "0.3"; }


	/* An empty default constructor is required to comply with BOECLIService */
	
	public GeographicIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(String value) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addGeographic(value, text.substring(1, text.length()-1));
		
		yypushback(1);	
	}
	
	

%} 

/* Identification of geographic entities in the Italian language */ 


REGION = (regione)
CITY = (citt.)({SE}?(di))?



%x BE  

%%

/* EU Countries */

{NAD}{AUSTRIA}{NAD}			{ save("AT"); }
{NAD}{BELGIUM}{NAD}			{ save("BE"); }
{NAD}{BULGARIA}{NAD}		{ save("BG"); }
{NAD}{CROATIA}{NAD}			{ save("HR"); }
{NAD}{CYPRUS}{NAD}			{ save("CY"); }
{NAD}{CZECH_REP}{NAD}		{ save("CZ"); }
{NAD}{DENMARK}{NAD}			{ save("DK"); }
{NAD}{ESTONIA}{NAD}			{ save("EE"); }
{NAD}{FINLAND}{NAD}			{ save("FI"); }
{NAD}{FRANCE}{NAD}			{ save("FR"); }
{NAD}{GERMANY}{NAD}			{ save("DE"); }
{NAD}{GREECE}{NAD}			{ save("GR"); }
{NAD}{HUNGARY}{NAD}			{ save("HU"); }
{NAD}{IRELAND}{NAD}			{ save("IE"); }
{NAD}{ITALY}{NAD}			{ save("IT"); }
{NAD}{LATVIA}{NAD}			{ save("LV"); }
{NAD}{LITHUANIA}{NAD}		{ save("LT"); }
{NAD}{LUXEMBOURG}{NAD}		{ save("LU"); }
{NAD}{MALTA}{NAD}			{ save("MT"); }
{NAD}{NETHERLANDS}{NAD}		{ save("NL"); }
{NAD}{POLAND}{NAD}			{ save("PL"); }
{NAD}{PORTUGAL}{NAD}		{ save("PT"); }
{NAD}{ROMANIA}{NAD}			{ save("RO"); }
{NAD}{SLOVAKIA}{NAD}		{ save("SK"); }
{NAD}{SLOVENIA}{NAD}		{ save("SI"); }
{NAD}{SPAIN}{NAD}			{ save("ES"); }
{NAD}{SWEDEN}{NAD}			{ save("SE"); }
{NAD}{UTD_KINGDOM}{NAD}		{ save("GB"); }


{NAD}{ALBANIA}{NAD}			{ save("AL"); }
{NAD}{ANDORRA}{NAD}			{ save("AD"); }
{NAD}{ARMENIA}{NAD}			{ save("AM"); }
{NAD}{AZERBAIJAN}{NAD}		{ save("AZ"); }
{NAD}{BOSNIA}{NAD}			{ save("BA"); }
{NAD}{GEORGIA}{NAD}			{ save("GE"); }
{NAD}{ICELAND}{NAD}			{ save("IS"); }
{NAD}{LIECHTENSTEIN}{NAD}	{ save("LI"); }
{NAD}{MACEDONIA}{NAD}		{ save("MK"); }
{NAD}{MONACO}{NAD}			{ save("MC"); }
{NAD}{MONTENEGRO}{NAD}		{ save("ME"); }
{NAD}{NORWAY}{NAD}			{ save("NO"); }
{NAD}{REP_MOLDOVA}{NAD}		{ save("MD"); }
{NAD}{RUSSIA}{NAD}			{ save("RU"); }
{NAD}{SANMARINO}{NAD}		{ save("SM"); }
{NAD}{SERBIA}{NAD}			{ save("CS"); }
{NAD}{SWITZERLAND}{NAD}		{ save("CH"); }
{NAD}{TURKEY}{NAD}			{ save("TR"); }
{NAD}{UKRAINE}{NAD}			{ save("UA"); }


{NAD}{BELARUS}{NAD}			{ save("BY"); }


/* National regions and cities */

{NAD}({REGION}{SE}?)?{ABRUZZO}{NAD}			{ save("IT_ABR"); }
{NAD}({REGION}{SE}?)?{BASILICATA}{NAD}		{ save("IT_BAS"); }
{NAD}({REGION}{SE}?)?{CALABRIA}{NAD}		{ save("IT_CAL"); }
{NAD}({REGION}{SE}?)?{CAMPANIA}{NAD}		{ save("IT_CAM"); }
{NAD}({REGION}{SE}?)?{EMILIA}{NAD}			{ save("IT_EMR"); }
{NAD}({REGION}{SE}?)?{FRIULI}{NAD}			{ save("IT_FVG"); }
{NAD}({REGION}{SE}?)?{LAZIO}{NAD}			{ save("IT_LAZ"); }
{NAD}({REGION}{SE}?)?{LIGURIA}{NAD}			{ save("IT_LIG"); }
{NAD}({REGION}{SE}?)?{LOMBARDIA}{NAD}		{ save("IT_LOM"); }
{NAD}({REGION}{SE}?)?{MARCHE}{NAD}			{ save("IT_MAR"); }
{NAD}({REGION}{SE}?)?{MOLISE}{NAD}			{ save("IT_MOL"); }
{NAD}({REGION}{SE}?)?{PIEMONTE}{NAD}		{ save("IT_PIE"); }
{NAD}({REGION}{SE}?)?{PUGLIA}{NAD}			{ save("IT_PUG"); }
{NAD}({REGION}{SE}?)?{SARDEGNA}{NAD}		{ save("IT_SAR"); }
{NAD}({REGION}{SE}?)?{SICILIA}{NAD}			{ save("IT_SIC"); }
{NAD}({REGION}{SE}?)?{TOSCANA}{NAD}			{ save("IT_TOS"); }
{NAD}({REGION}{SE}?)?{TRENTINO}{NAD}		{ save("IT_TAA"); }
{NAD}({REGION}{SE}?)?{UMBRIA}{NAD}			{ save("IT_UMB"); }
{NAD}({REGION}{SE}?)?{VAOSTA}{NAD}			{ save("IT_VDA"); }
{NAD}({REGION}{SE}?)?{VENETO}{NAD}			{ save("IT_VEN"); }


{NAD}({CITY}{SE}?)?{AQUILA}{NAD}			{ save("IT_AQ"); }
{NAD}({CITY}{SE}?)?{POTENZA}{NAD}			{ save("IT_PZ"); }
{NAD}({CITY}{SE}?)?{CATANZARO}{NAD}			{ save("IT_CZ"); }
{NAD}({CITY}{SE}?)?{NAPOLI}{NAD}			{ save("IT_NA"); }
{NAD}({CITY}{SE}?)?{BOLOGNA}{NAD}			{ save("IT_BO"); }
{NAD}({CITY}{SE}?)?{TRIESTE}{NAD}			{ save("IT_TS"); }
{NAD}({CITY}{SE}?)?{ROMA}{NAD}				{ save("IT_RM"); }
{NAD}({CITY}{SE}?)?{GENOVA}{NAD}			{ save("IT_GE"); }
{NAD}({CITY}{SE}?)?{MILANO}{NAD}			{ save("IT_MI"); }
{NAD}({CITY}{SE}?)?{ANCONA}{NAD}			{ save("IT_AN"); }
{NAD}({CITY}{SE}?)?{CAMPOBASSO}{NAD}		{ save("IT_CB"); }
{NAD}({CITY}{SE}?)?{TORINO}{NAD}			{ save("IT_TO"); }
{NAD}({CITY}{SE}?)?{BARI}{NAD}				{ save("IT_BA"); }
{NAD}({CITY}{SE}?)?{CAGLIARI}{NAD}			{ save("IT_CA"); }
{NAD}({CITY}{SE}?)?{PALERMO}{NAD}			{ save("IT_PA"); }
{NAD}({CITY}{SE}?)?{FIRENZE}{NAD}			{ save("IT_FI"); }
{NAD}({CITY}{SE}?)?{TRENTO}{NAD}			{ save("IT_TN"); }
{NAD}({CITY}{SE}?)?{PERUGIA}{NAD}			{ save("IT_PG"); }
{NAD}({CITY}{SE}?)?{AOSTA}{NAD}				{ save("IT_AO"); }
{NAD}({CITY}{SE}?)?{VENEZIA}{NAD}			{ save("IT_VE"); }



/* Capoluoghi di provincia coinvolti nei TAR */

{NAD}({CITY}{SE}?)?{PESCARA}{NAD}			{ save("IT_PE"); }
{NAD}({CITY}{SE}?)?{REGGIOCAL}{NAD}			{ save("IT_RC"); }
{NAD}({CITY}{SE}?)?{SALERNO}{NAD}			{ save("IT_SA"); }
{NAD}({CITY}{SE}?)?{PARMA}{NAD}				{ save("IT_PR"); }
{NAD}({CITY}{SE}?)?{LATINA}{NAD}			{ save("IT_LT"); }
{NAD}({CITY}{SE}?)?{BRESCIA}{NAD}			{ save("IT_BS"); }
{NAD}({CITY}{SE}?)?{LECCE}{NAD}				{ save("IT_LE"); }
{NAD}({CITY}{SE}?)?{CATANIA}{NAD}			{ save("IT_CT"); }
{NAD}({CITY}{SE}?)?{BOLZANO}{NAD}			{ save("IT_BZ"); }

/* TODO terminare la lista delle città capoluogo di provincia */


/* Municipalies (Comuni) */

{NAD}({CITY}{SE}?)?{VERBANIA}{NAD}			{ save("IT_L746"); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]    				{ addText(yytext()); }


