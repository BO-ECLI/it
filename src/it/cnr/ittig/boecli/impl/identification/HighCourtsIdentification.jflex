/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CaseLawAuthority;
import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.NationalCaseLawAuthorities;
import it.cnr.ittig.boecli.impl.Config;


%%
%class HighCourtsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in


%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "National High Courts identification"; }

	@Override
	public String version() { return "0.7"; }
		

	/* An empty default constructor is required to comply with BOECLIService */
	
	public HighCourtsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(CaseLawAuthority authority) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addAuthority(authority, text.substring(1, text.length()-1));
		
		yypushback(1);
	}


%} 


/* Corte suprema di cassazione */

COURT = (corte)|(c\.)
SUPREME = (suprema)
CASSATION = (cassazione)|(cassaz\.?)|(cass\.?)
COURT_OF_CASSATION_1 = ({COURT}{SE}?{SUPREME}?{SE}?{OF}?{SE}?)?{CASSATION}
COURT_OF_CASSATION_2 = {COURT}{SE}?{SUPREME}
COURT_OF_CASSATION = {COURT_OF_CASSATION_1}|{COURT_OF_CASSATION_2}


/* Corte Costituzionale */

COST = (cost\.?)
CONSTITUTIONAL = (costituzionale)|(costituz\.?)
CONSTITUTIONAL_COURT_1 = {COURT}{SE}?{CONSTITUTIONAL}
CONSTITUTIONAL_COURT_2 = {COURT}{SE}?{COST}
CONSTITUTIONAL_COURT = {CONSTITUTIONAL_COURT_1}|{CONSTITUTIONAL_COURT_2}


/* Corte dei conti */

AUDIT = (conti)
AUDIT_COURT = {COURT}{SE}?{OF}?{SE}?{AUDIT}


/* Consiglio di stato */

COUNCIL = (consiglio)|(cons\.?)
STATE = (stato)
COUNCIL_OF_STATE = {COUNCIL}({SE}?{OF})?{SE}?{STATE}

/* Consiglio di giustizia amministrativa per la Regione Siciliana */
GIUSTIZIA = (giustizia)|(giust\.?)
AMMINISTRATIVA = (amministrativa)|(amm\.?)
REGIONE = (regione)|(reg\.?)
SICILIA = (sicilia)|(siciliana)|(sic\.)
CGARS = {COUNCIL}{SE}?{GIUSTIZIA}{SE}?{AMMINISTRATIVA}{SE}?{REGIONE}?{SE}?{SICILIA}


%x BE   

%%


{NAD}{COURT_OF_CASSATION}{NAD}				{ save(NationalCaseLawAuthorities.IT_CASS); }

{NAD}{CONSTITUTIONAL_COURT}{NAD}			{ save(NationalCaseLawAuthorities.IT_COST); }

{NAD}{AUDIT_COURT}{NAD}						{ save(NationalCaseLawAuthorities.IT_CONT); }

{NAD}{COUNCIL_OF_STATE}{NAD}				{ save(NationalCaseLawAuthorities.IT_CDS); }

{NAD}{CGARS}{NAD}							{ save(NationalCaseLawAuthorities.IT_CGARS); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]    				{ 
						addText(yytext());
					}


