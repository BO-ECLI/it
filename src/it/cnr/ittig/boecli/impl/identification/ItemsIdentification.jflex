/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class ItemsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in



%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Item partition elements identification"; }

	@Override
	public String version() { return "0.2"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public ItemsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private int offset = 0;
	private int length = 0;
	
	private void start() {
	
		offset = 0;
		length = yylength();
		
		yypushback(length);
		
		yybegin(itemState);
		
	}
	
	private static String NUMBER = "[BOECLI:NUMBER:";
	
	private void save(String text) {
		
		int itemStart = text.indexOf(NUMBER);
		
		String itemValue = "";
		
		if(itemStart > -1) {
			
			itemValue = text.substring(itemStart + NUMBER.length(), text.substring(itemStart).indexOf("]") + itemStart);
		}
	
		addItem(itemValue, yytext());
	}
	
%} 


/* [BOECLI:NUMBER:ddd] is converted to [BOECLI:ITEM:ddd] when adjacent to another partition element or to a legislation alias */

BE_NUM = {BOPEN}(NUMBER:)[0-9]+\]{BE_BODY}

ALIAS_ITEM = {BE_LEGISLATIONALIAS}({BE_SEP}?{BE_NUM})+

PAR_ITEM = {BE_PARAGRAPH}({BE_SEP}?{BE_NUM})+
LET_ITEM = {BE_LETTER}({BE_SEP}?{BE_NUM})+
ART_ITEM = {BE_ARTICLE}({BE_SEP}?{BE_NUM})+

ITEM_PAR = ({BE_NUM}{BE_SEP}?)+{BE_PARAGRAPH}
ITEM_LET = ({BE_NUM}{BE_SEP}?)+{BE_LETTER}
ITEM_ART = ({BE_NUM}{BE_SEP}?)+{BE_ARTICLE}



/* Exceptions */

EX_1 = {BE_DATE}{BE_SEP}?{BE_NUMBER}
EX_2 = {BE_TYPE}{BE_SEP}?{BE_NUMBER}

EXCEPTIONS = {EX_1}|{EX_2}

%x BE itemState     

%%

{EXCEPTIONS} 	{ addText(yytext()); }

{ALIAS_ITEM}|{PAR_ITEM}|{LET_ITEM}|{ART_ITEM}|{ITEM_PAR}|{ITEM_LET}|{ITEM_ART}		{ start(); }


<itemState> {

	{BE_NUM}		{ save(yytext()); offset += yylength(); }
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
				
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}


{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{
						addText(yytext());
					}


