/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class NumbersIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Numbers"; }

	@Override
	public String version() { return "0.7"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public NumbersIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private String year = "";
	private String number = "";
	private String numberText = "";
	
	private int offset = 0;
	private int length = 0;
	
	private static final Pattern digits = Pattern.compile("\\d+");
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */


	private void save(boolean yearFirst) {
		
		String text = yytext();
		
		addText(text.substring(0,1));
		
		text = text.substring(1, text.length()-1);
		
		Matcher matcher = digits.matcher(text);
		
		matcher.find();
		
		String first = text.substring(matcher.start(), matcher.end());
		
		if( !matcher.find()) {
		
			addNumber(first, text);
			yypushback(1);
			return;
		}
		
		String second = text.substring(matcher.start(), matcher.end());		
		
		if(yearFirst) {
		
			addNumber(second, first, text);
		
		} else {
		
			addNumber(first, second, text);
		}
		
		yypushback(1);
	}
	
	
	private String getYearAsLastNumber() {
		
		String text = yytext();
		
		Matcher matcher = digits.matcher(text);
		
		String out = "";
		
		while( matcher.find() ) {
			out = text.substring(matcher.start(), matcher.end());
		}
		
		return out;
	}
	
%} 


PREFIX_SINGLE = (numero)|(num\.?)|(nr\.?)|(n\.?)|(no\.?)|(n°)
PREFIX_MULTIPLE = (numeri)|(num\.?)|(nr\.?)|(n\.?)|(nn\.?)

PREFIX = {PREFIX_SINGLE}|{PREFIX_MULTIPLE}

NUMBERYEAR_SEP = {YEAR_PREFIX}|{SLASHES}

NUMBERYEAR = {DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}




/* Number composed by two numbers and a year: "la sentenza n. 152/13/99"  */
NUMNUMYEAR = {DD}{SLASHES}{DD}{SLASHES}{YEAR}


%x BE nny 

%%


{NAD}{PREFIX}{SE}?{DD}{ND}	{ save(false); }



/* 1999/15 */
{NAD}({PREFIX}{SE}?)?{YEARYYYY}{SLASHES}{YEARYY}{ND}	{ save(true); }

/* DD/YYYY, DD/YY */
{NAD}({PREFIX}{SE}?)?{NUMBERYEAR}{ND}					{ save(false); }
										
/* 1999/123 */
{NAD}({PREFIX}{SE}?)?{YEARYYYY}{SLASHES}{DD}{ND}		{ save(true); }

/* 99/123 */
{NAD}({PREFIX}{SE}?)?{YEARYY}{SLASHES}{DD}{ND}			{ save(true); }




{NAD}({PREFIX_SINGLE}{SE}?)?{NUMNUMYEAR}{ND}	{ 
													year = getYearAsLastNumber();
													number = "";
													addText(yytext().substring(0,1));
													numberText = yytext().substring(1,yylength()-1);
													offset = 1;
													length = yylength();
													yypushback(length-1);
													yybegin(nny);
												}

<nny> {
	
	{DD}{SLASHES}{DD}	{ number = yytext(); offset += yylength(); }

	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					addNumber(number, year, numberText);
								
					year = "";
					number = "";
					yypushback(1);
					yybegin(YYINITIAL);
				}
			}
}


{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]					{ addText(yytext()); }


