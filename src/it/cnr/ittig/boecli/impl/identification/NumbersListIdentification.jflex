/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class NumbersListIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Numbers List"; }

	@Override
	public String version() { return "0.6"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public NumbersListIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private String year = "";
	private String number = "";
	private String numberText = "";
	
	private int offset = 0;
	private int length = 0;
	
	private static final Pattern digits = Pattern.compile("\\d+");
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(String text) {
	
		save("", text);
	}	
	
	private void save(String prefix, String text) {

		Matcher matcher = digits.matcher(text);
		
		if( !matcher.find()) {
			
			//TODO error
			addText(text);
			return;
		}
		
		String num = text.substring(matcher.start(), matcher.end());

		addNumber(prefix, num, year, text);
	}
	
	private String getYearAsLastNumber() {
		
		String text = yytext();
		
		Matcher matcher = digits.matcher(text);
		
		String out = "";
		
		while( matcher.find() ) {
			out = text.substring(matcher.start(), matcher.end());
		}
		
		return out;
	}
	
	private String getYearAsFirstNumber() {
		
		String text = yytext();
		
		Matcher matcher = digits.matcher(text);
		
		String out = "";
		
		if( matcher.find() ) {
			out = text.substring(matcher.start(), matcher.end());
		}
		
		return out;
	}
	
	private void saveYearFirst(String prefix, String text) {

		Matcher matcher = digits.matcher(text);
		
		String num = "";
		
		while( matcher.find() ) {
			num = text.substring(matcher.start(), matcher.end());
		}
		
		addNumber(prefix, num, year, text);
	}
	
%} 


PREFIX_SINGLE = (numero)|(num\.?)|(nr\.?)|(n\.?)|(no\.?)|(n°)
PREFIX_MULTIPLE = (numeri)|(num\.?)|(nr\.?)|(n\.?)|(nn\.?)

PREFIX = {PREFIX_SINGLE}|{PREFIX_MULTIPLE}

NUMBERYEAR_SEP = {YEAR_PREFIX}|{SLASHES}

NUMBERYEAR = ({PREFIX}{SE}?)?{DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}


BR_DD = \(?{DD}\)?

/* List of numbers (size > 1) separated by commas and conjunctions */

NumberList1 = {BR_DD}({SCE}({PREFIX}{SE}?)?{BR_DD})+

NumberList2 = {BR_DD}{SE}?{AND}{SE}?({PREFIX}{SE}?)?{BR_DD}

NumberList3 = {BR_DD}({SCE}({PREFIX}{SE}?)?{BR_DD})+{SE}?{AND}{SE}?({PREFIX}{SE}?)?{BR_DD}

NumberList = {NumberList1}|{NumberList2}|{NumberList3}


/* multiple number, no year */
NumberListExp = {PREFIX}{SE}?{NumberList}


NumberYearExp = ({PREFIX}{SE}?)?{DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}

/* multiple number, same year */
NumberYearListExp = ({PREFIX}{SE}?)?{NumberList}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}


/* multiple year/number */
YearNumber = {YEAR}{SE}?{SLASHES}{SE}?{DD}
YearNumberList = {PREFIX}{SE}?{YearNumber}({SCE}?{AND}?{SE}?{YearNumber})+


%x BE numberYearList numberList yearNumberList 

%%

{NAD}{NumberListExp}{ND}	{
								addText(yytext().substring(0,1));
								offset = 1;
								year = "";
								length = yylength();
								yypushback(length-1);
								yybegin(numberList);
							}

<numberList> {

	({PREFIX}{SE}?)?{BR_DD}		{
									save(yytext());
									offset += yylength();
								}
																
	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					yypushback(1);
					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}


({NAD}{NumberYearListExp}{ND})|
({NAD}{NumberYearExp}{ND})			{
										year = getYearAsLastNumber();
										addText(yytext().substring(0,1));
										offset = 1;
										length = yylength();
										yypushback(length-1);
										yybegin(numberYearList);
									}

<numberYearList> {

	{PREFIX}{SE}?{BR_DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}		{
																	save(yytext());
																	offset += yylength();
																}
																
	{PREFIX}{SE}?{BR_DD}	{
								save(yytext());
								offset += yylength();
							}
																
																
	{BR_DD}{SE}?{NUMBERYEAR_SEP}{SE}?{YEAR}		{
													save(yytext());
													offset += yylength();
												}
																
	{BR_DD}		{
					save(yytext());
					offset += yylength();
				}
																
																
	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					year = "";
					yypushback(1);
					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}


{NAD}{YearNumberList}{ND}			{
										year = getYearAsFirstNumber();
										addText(yytext().substring(0,1));
										offset = 1;
										length = yylength();
										yypushback(length-1);
										yybegin(yearNumberList);
									}

<yearNumberList> {

	({PREFIX}{SE}?)?{YearNumber}		{
											saveYearFirst("", yytext());
											offset += yylength();
										}

	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					year = "";
					yypushback(1);
					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]					{ addText(yytext()); }


