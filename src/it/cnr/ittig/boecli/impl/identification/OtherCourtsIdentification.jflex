/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CaseLawAuthority;
import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.NationalCaseLawAuthorities;
import it.cnr.ittig.boecli.impl.Config;


%%
%class OtherCourtsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in


%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Other national courts identification"; }

	@Override
	public String version() { return "0.7"; }
		

	/* An empty default constructor is required to comply with BOECLIService */
	
	public OtherCourtsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	int offset = 0;
	int length = 0;

	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(CaseLawAuthority authority) {
		
		save(authority, false);
	}
	
	private void save(CaseLawAuthority authority, boolean back) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		if(back) { 
			
			addAuthority(authority, text.substring(1, text.length()-1));
			yypushback(1);
		
		} else {
		
			addAuthority(authority, text.substring(1));
		}
	}

%} 


/* Identification of case-law authorities in the Italian language */


GeoOpen = {BOPEN}(GEO:)
SecOpen = {BOPEN}(SECTION:)
LocOpen = {GeoOpen}|{SecOpen}

/* Corte d'Appello */
COURT = (corte)
APPEAL = (appello)
COURT_APPEAL = {COURT}{SE}?{OF}?{SE}?{APPEAL}


/* Tribunale Amministrativo Regionale */

TRIBUNAL_EXT = (tribunale) /* |(trib\.?)  Watch out for "tributaria" */
TRIBUNAL = {TRIBUNAL_EXT}|(t\.)

ADMINISTRATIVE = (amministrativo)|(ammin\.?)|(amm\.?)|(a\.?)
REGIONAL = (regionale)|(regione)|(reg\.?)|(r\.?)
TAR = {TRIBUNAL}{SE}?{ADMINISTRATIVE}{SE}?{REGIONAL}


/* Commissione tributaria regionale e provinciale */

COMMISSION = (commissione)|(comm\.?)
TRIB = (tributaria)|(trib\.?)
PROV = (provinciale)|(prov\.?)

COMM_TRIB_REG = {COMMISSION}{SE}?{TRIB}({SE}?{REGIONAL})?
COMM_TRIB_PROV = {COMMISSION}{SE}?{TRIB}({SE}?{PROV})?

/* Pretura */
PRETURA = (pretura)|(pretore)


%x BE   

%%

{NAD}{TRIBUNAL_EXT}{NAD}				{ save(NationalCaseLawAuthorities.IT_TRB, true); }

/* ({NAD}{TRIBUNAL_EXT})/({SDHE}?{OF}?{SE}?{GeoOpen}(IT_))				{ save(NationalCaseLawAuthorities.IT_TRB); } */
/* ({NAD}{TRIBUNAL_EXT})/({SDHE}?{OF}?{SE}?{GeoOpen}(IT_L746)\])			{ save(NationalCaseLawAuthorities.IT_TRBL746); } */



{NAD}{PRETURA}{NAD}						{ save(NationalCaseLawAuthorities.IT_PTR, true); }



{NAD}{COURT_APPEAL}{NAD}				{ save(NationalCaseLawAuthorities.IT_CPP, true); }


({NAD}{COURT_APPEAL})/({SDHE}?{OF}?{SE}?{GeoOpen}(IT_TO)\])			{ save(NationalCaseLawAuthorities.IT_CPPTO); }
({NAD}{COURT_APPEAL})/({SDHE}?{OF}?{SE}?{GeoOpen}(IT_NA)\])			{ save(NationalCaseLawAuthorities.IT_CPPNA); }



({NAD}{TAR})/({NAD})							{ save(NationalCaseLawAuthorities.IT_TAR); }


({NAD}{TAR})/({SDHE}?{OF}?{SE}?{LocOpen}(IT_TOS)\])			{ save(NationalCaseLawAuthorities.IT_TARFI); }
({NAD}{TAR})/({SDHE}?{OF}?{SE}?{LocOpen}(IT_FI)\])			{ save(NationalCaseLawAuthorities.IT_TARFI); }


({NAD}{TAR})/({SDHE}?{OF}?{SE}?{LocOpen}(IT_LAZ)\])													{ save(NationalCaseLawAuthorities.IT_TARRM); }
({NAD}{TAR})/({SDHE}?{OF}?{SE}?{LocOpen}(IT_RM)\])													{ save(NationalCaseLawAuthorities.IT_TARRM); }
({NAD}{TAR})/({SDHE}?{OF}?{SE}?{GeoOpen}(IT_LAZ)\]{BE_BODY}{SDHE}?{OF}?{SE}?{SecOpen}(IT_LT)[\]])		{ save(NationalCaseLawAuthorities.IT_TARLT); }
({NAD}{TAR})/({SDHE}?{OF}?{SE}?{LocOpen}(IT_LT)\])													{ save(NationalCaseLawAuthorities.IT_TARLT); }



{NAD}{COMM_TRIB_REG}{NAD}							{ save(NationalCaseLawAuthorities.IT_CTR, true); }
{NAD}{COMM_TRIB_PROV}{NAD}							{ save(NationalCaseLawAuthorities.IT_CTP, true); }


({NAD}{COMM_TRIB_PROV})/({SCE}?({OF}?{SE}?)?{GeoOpen}(IT_TO)\])			{ save(NationalCaseLawAuthorities.IT_CTPTO); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]    				{ 
						addText(yytext());
					}


