/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class PartitionElementsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in



%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Partitions Elements identification"; }

	@Override
	public String version() { return "1.1"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public PartitionElementsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private int offset = 0;
	private int length = 0;
	
	private int offsetLatin = 0;
	private int lengthLatin = 0;
	private String textLatin = "";
	private String value = "";
	private String latin = "";
	
	private int prevState = YYINITIAL;


	private void inList() {
	
		offset++;
		
		if( offset >= length ) {
			yypushback(1);
			yybegin(YYINITIAL);
		}
		
		addText(yytext());
	}
	
	private void startPartition(int partitionState) {
	
		addText(yytext().substring(0,1));
		offset = 1;
		length = yylength();
		yypushback(length-1);
		yybegin(partitionState);
	}
	
	private void startLatin() {
	
		offsetLatin = 0;
		value = "";
		latin = "";
		lengthLatin = yylength();
		offset += lengthLatin;
		prevState = yystate();
		
		textLatin = yytext();
		yypushback(lengthLatin);
		yybegin(Latin);
	}
	
	private static final Pattern digits = Pattern.compile("\\d+");
	
	private String readNumber() {
	
		Matcher matcher = digits.matcher(yytext());
		String number = "";
		
		while(matcher.find()) {
			
			number = yytext().substring(matcher.start(), matcher.end());			
			break;
		}
		
		return number;
	}
%} 

/* Latin numerals (up to 25th) */

Lat1 = (semel)
Lat2 = (bis)
Lat3 = (ter)
Lat4 = (quater)
Lat5 = (quinquies)
Lat6 = (sexies)
Lat7 = (septies)
Lat8 = (octies)
Lat9 = (novies)
Lat10 = (decies)
Lat11 = (undecies)
Lat12 = (duodecies)
Lat13 = (ter[ ]?decies)
Lat14 = (quater[ ]?decies)
Lat15 = (quinquies[ ]?decies)|(quindecies)
Lat16 = (sexies[ ]?decies)|(sexdecies)
Lat17 = (septies[ ]?decies)|(septdecies)
Lat18 = (octies[ ]?decies)|(octodecies)
Lat19 = (novies[ ]?decies)|(novodecies)
Lat20 = (vicies)
Lat21 = (vicies[ ]?semel)|(unvicies)
Lat22 = (vicies[ ]?bis)|(duovicies)
Lat23 = (vicies[ ]?ter)|(tervicies)
Lat24 = (vicies[ ]?quater)|(quatervicies)
Lat25 = (vicies[ ]?quinquies)|(quinvicies)

Latin = {Lat1}|{Lat2}|{Lat3}|{Lat4}|{Lat5}|{Lat6}|{Lat7}|{Lat8}|{Lat9}|{Lat10}|{Lat11}|{Lat12}|{Lat13}|{Lat14}|{Lat15}|{Lat16}|{Lat17}|{Lat18}|{Lat19}|{Lat20}|{Lat21}|{Lat22}|{Lat23}|{Lat24}|{Lat25}


ord1 = {Ord1}|(1[\^°ºªa])
ord2 = {Ord2}|(2[\^°ºªa])
ord3 = {Ord3}|(3[\^°ºªa])
ord4 = {Ord4}|(4[\^°ºªa])
ord5 = {Ord5}|(5[\^°ºªa])
ord6 = {Ord6}|(6[\^°ºªa])
ord7 = {Ord7}|(7[\^°ºªa])
ord8 = {Ord8}|(8[\^°ºªa])
ord9 = {Ord9}|(9[\^°ºªa])
ord10 = {Ord10}|(10[\^°ºªa])
ord11 = {Ord11}|(11[\^°ºªa])
ord12 = {Ord12}|(12[\^°ºªa])
ord13 = {Ord13}|(13[\^°ºªa])
ord14 = {Ord14}|(14[\^°ºªa])
ord15 = {Ord15}|(15[\^°ºªa])
ord16 = {Ord16}|(16[\^°ºªa])
ord17 = {Ord17}|(17[\^°ºªa])
ord18 = {Ord18}|(18[\^°ºªa])
ord19 = {Ord19}|(19[\^°ºªa])
ord20 = {Ord20}|(20[\^°ºªa])
ord21 = {Ord21}|(21[\^°ºªa])
ord22 = {Ord22}|(22[\^°ºªa])
ord23 = {Ord23}|(23[\^°ºªa])
ord24 = {Ord24}|(24[\^°ºªa])
ord25 = {Ord25}|(25[\^°ºªa])

ordLast = (ultimo)

ords = {ord1}|{ord2}|{ord3}|{ord4}|{ord5}|{ord6}|{ord7}|{ord8}|{ord9}|{ord10}|{ord11}|{ord12}|{ord13}|{ord14}|{ord15}|{ord16}|{ord17}|{ord18}|{ord19}|{ord20}|{ord21}|{ord22}|{ord23}|{ord24}|{ord25}|{ordLast}




/* Numeric references for partitions with optional latin specification */
/* "2" "2 bis" "2 ter)" "2-quater" "3 - quinquies )" */

NumLatinSuffix = ({DH}{SE}?)?{Latin}({SE}?[\)])? 
NumLatin = {DD}({SE}?{NumLatinSuffix})?

Following = (seguent.)|(seg\.?)|(segg\.?)
FollowingSuffix = ({AND}{SE}?)?{Following}

/* Multiples: "1, 2 AND 3" */ 

NumLatinAndNumLatin = {NumLatin}{SE}?{AND}{SE}?{NumLatin}

NumLatinNumLatinAndNumLatin = {NumLatin}({SCDHE}{NumLatin})+({SE}?{AND}{SE}?{NumLatin})?

NumLatinList = ({NumLatin}|{NumLatinAndNumLatin}|{NumLatinNumLatinAndNumLatin})({SE}?{FollowingSuffix})?



OrdAndOrd = {ords}{SE}?{AND}{SE}?{ords}

OrdOrdAndOrd = {ords}({SCE}{ords})+({SE}?{AND}{SE}?{ords})?

OrdinalList = ({ords}|{OrdAndOrd}|{OrdOrdAndOrd})


/* Letter references for partitions with optional latin specification */
/* "a" "b)" "a-bis)"  */

Lett = [a-zA-Z][a-zA-Z]?

LettLatinSuffix = ({DH}{SE}?)?{Latin}
LettLatinNoBracket = {Lett}({SE}?{LettLatinSuffix})?
LettLatinBracket = {Lett}({SE}?{LettLatinSuffix})?({SE}?[\)])
LettLatin = {LettLatinNoBracket}|{LettLatinBracket}

/* Multiples: "a,b,c" or "a), b) AND c)" */

LettLatinAndLettLatin = {LettLatinBracket}{SE}?{AND}{SE}?{LettLatinBracket}

LettLatinLettLatin = {LettLatinNoBracket}({SCE}{LettLatinNoBracket})+

LettLatinLettLatinAndLettLatin = {LettLatinBracket}({SCE}{LettLatinBracket})+({SE}?{AND}{SE}?{LettLatinBracket})?

LettLatinListBracket = ({LettLatinBracket}|{LettLatinAndLettLatin}|{LettLatinLettLatinAndLettLatin})({SE}?{FollowingSuffix})?

LettLatinListNoBracket = ({LettLatinNoBracket}|{LettLatinLettLatin})({SE}?{FollowingSuffix})?



/**************/
/* Partitions */
/**************/
EX_PREFIX = (ex)

ARTICLE = ({EX_PREFIX}{SE}?)?((articol.)|(art\.?)|(artt\.?))

COMMA = ({EX_PREFIX}{SE}?)?((comma)|(commi))

LETTER = ({EX_PREFIX}{SE}?)?((letter.)|(let\.?)|(lett\.?))

PARAGRAPH = ({EX_PREFIX}{SE}?)?((paragraf.)|(par\.?)|(punto)|(punti)|(§))


/* art. 1, 2, terzo comma, 3 e 4 */
COMMA_ORD = {ords}{SE}?{COMMA}

/* ART_COMMA_ORD = {ARTICLE}{SE}?{DD}({SCE}?{COMMA_ORD})?({SCE}?{DD}({SCE}?{COMMA_ORD})?)+({SE}?{AND}{SE}?{DD}({SCE}?{COMMA_ORD})?)? */
ART_COMMA_ORD = {ARTICLE}{SE}?{DD}({SCE}?{AND}?{SE}?{COMMA_ORD})*({SCE}?{AND}?{SE}?{DD}({SCE}?{AND}?{SE}?{COMMA_ORD})*)*


/* artt. 2043 cod. civ. e 185 cod. pen. */ 

ART_ALIAS_ART_SUB =  {SCDHE}?{COMMA_ORD}({SCDHE}?{LETTER}{SE}?{LettLatinBracket})?
ART_ALIAS_ART = {ARTICLE}{SE}?{DD}{ART_ALIAS_ART_SUB}?({SCE}?{AND}?{SE}?{DD}{ART_ALIAS_ART_SUB}?)*{SCE}?{OF}?{SE}?{BE_LEGISLATIONALIAS}({SCE}?{AND}?{SE}?{DD}{ART_ALIAS_ART_SUB}?)+


/* art. 1, 1^ comma, e 2 della legge... */



%x BE articleList commaList commaOrdList paragraphList letterListBracket letterListNoBracket Latin artCommaOrd artAliasArt 

%%


{NAD}{OrdinalList}{SE}?{COMMA}{NAD}					{ startPartition(commaOrdList); }

{NAD}{ARTICLE}{SE}?{NumLatinList}{NAD}				{ startPartition(articleList); }

{NAD}{COMMA}{SE}?{NumLatinList}{NAD}				{ startPartition(commaList); }

{NAD}{PARAGRAPH}{SE}?{NumLatinList}{NAD}			{ startPartition(paragraphList); }

{NAD}{LETTER}{SE}?{LettLatinListBracket}{NAD}		{ startPartition(letterListBracket); }

{NAD}{LETTER}{SE}?{LettLatinListNoBracket}{NAD}		{ startPartition(letterListNoBracket); }


<articleList>	{
	
	{ARTICLE}{SE}?{NumLatin}({SE}?{FollowingSuffix})? 		{ startLatin(); }
						
	{NumLatin}({SE}?{FollowingSuffix})?						{ startLatin(); }
						
	[^]		{ inList(); }
}

<commaList>	{
	
	{COMMA}{SE}?{NumLatin}({SE}?{FollowingSuffix})? 		{ startLatin(); }
						
	{NumLatin}({SE}?{FollowingSuffix})?						{ startLatin(); }
						
	[^]		{ inList(); }
}

<commaOrdList>	{

	{ord1}({SE}?{COMMA})?		{ addParagraph("1", yytext()); offset += yylength(); }
	{ord2}({SE}?{COMMA})?		{ addParagraph("2", yytext()); offset += yylength(); }
	{ord3}({SE}?{COMMA})?		{ addParagraph("3", yytext()); offset += yylength(); }
	{ord4}({SE}?{COMMA})?		{ addParagraph("4", yytext()); offset += yylength(); }
	{ord5}({SE}?{COMMA})?		{ addParagraph("5", yytext()); offset += yylength(); }
	{ord6}({SE}?{COMMA})?		{ addParagraph("6", yytext()); offset += yylength(); }
	{ord7}({SE}?{COMMA})?		{ addParagraph("7", yytext()); offset += yylength(); }
	{ord8}({SE}?{COMMA})?		{ addParagraph("8", yytext()); offset += yylength(); }	
	{ord9}({SE}?{COMMA})?		{ addParagraph("9", yytext()); offset += yylength(); }
	{ord10}({SE}?{COMMA})?		{ addParagraph("10", yytext()); offset += yylength(); }	
	{ord11}({SE}?{COMMA})?		{ addParagraph("11", yytext()); offset += yylength(); }
	{ord12}({SE}?{COMMA})?		{ addParagraph("12", yytext()); offset += yylength(); }	
	{ord13}({SE}?{COMMA})?		{ addParagraph("13", yytext()); offset += yylength(); }
	{ord14}({SE}?{COMMA})?		{ addParagraph("14", yytext()); offset += yylength(); }	
	{ord15}({SE}?{COMMA})?		{ addParagraph("15", yytext()); offset += yylength(); }
	{ord16}({SE}?{COMMA})?		{ addParagraph("16", yytext()); offset += yylength(); }	
	{ord17}({SE}?{COMMA})?		{ addParagraph("17", yytext()); offset += yylength(); }
	{ord18}({SE}?{COMMA})?		{ addParagraph("18", yytext()); offset += yylength(); }	
	{ord19}({SE}?{COMMA})?		{ addParagraph("19", yytext()); offset += yylength(); }
	{ord20}({SE}?{COMMA})?		{ addParagraph("20", yytext()); offset += yylength(); }	
	{ord21}({SE}?{COMMA})?		{ addParagraph("21", yytext()); offset += yylength(); }
	{ord22}({SE}?{COMMA})?		{ addParagraph("22", yytext()); offset += yylength(); }	
	{ord23}({SE}?{COMMA})?		{ addParagraph("23", yytext()); offset += yylength(); }
	{ord24}({SE}?{COMMA})?		{ addParagraph("24", yytext()); offset += yylength(); }	
	{ord25}({SE}?{COMMA})?		{ addParagraph("25", yytext()); offset += yylength(); }
	
	{ordLast}({SE}?{COMMA})?	{ addStopword(yytext()); offset += yylength(); }
		
	[^]		{ inList(); }
}

<paragraphList>	{
	
	{PARAGRAPH}{SE}?{NumLatin}({SE}?{FollowingSuffix})? 	{ startLatin(); }
						
	{NumLatin}({SE}?{FollowingSuffix})?						{ startLatin(); }
						
	[^]		{ inList(); }
}


<letterListBracket>	{
	
	{LETTER}{SE}?{LettLatinBracket}({SE}?{FollowingSuffix})?	{ startLatin(); }
						
	{LettLatinBracket}({SE}?{FollowingSuffix})?					{ startLatin(); }

	[^]		{ inList(); }
}


<letterListNoBracket>	{
	
	{LETTER}{SE}?{LettLatinNoBracket}({SE}?{FollowingSuffix})? 		{ startLatin(); }
						
	{LettLatinNoBracket}({SE}?{FollowingSuffix})?					{ startLatin(); }
						
	[^]		{ inList(); }
}


<Latin>	{

	{DD}		{ 
					value = yytext();
					offsetLatin += yylength();
				}
				
	{Lett}		{ 
					value = yytext();
					offsetLatin += yylength();
				} 
	
	{Lat1}		{ latin = "SEMEL"; offsetLatin += yylength(); }
	{Lat2}		{ latin = "BIS"; offsetLatin += yylength(); }
	{Lat3}		{ latin = "TER"; offsetLatin += yylength(); }
	{Lat4}		{ latin = "QUATER"; offsetLatin += yylength(); }
	{Lat5}		{ latin = "QUINQUIES"; offsetLatin += yylength(); }
	{Lat6}		{ latin = "SEXIES"; offsetLatin += yylength(); }
	{Lat7}		{ latin = "SEPTIES"; offsetLatin += yylength(); }
	{Lat8}		{ latin = "OCTIES"; offsetLatin += yylength(); }
	{Lat9}		{ latin = "NOVIES"; offsetLatin += yylength(); }
	{Lat10}		{ latin = "DECIES"; offsetLatin += yylength(); }
	{Lat11}		{ latin = "UNDECIES"; offsetLatin += yylength(); }
	{Lat12}		{ latin = "DUODECIES"; offsetLatin += yylength(); }		
	{Lat13}		{ latin = "TERDECIES"; offsetLatin += yylength(); }
	{Lat14}		{ latin = "QUATERDECIES"; offsetLatin += yylength(); }
	{Lat15}		{ latin = "QUINQUIESDECIES"; offsetLatin += yylength(); }
	{Lat16}		{ latin = "SEXIESDECIES"; offsetLatin += yylength(); }
	{Lat17}		{ latin = "SEPTIESDECIES"; offsetLatin += yylength(); }
	{Lat18}		{ latin = "OCTIESDECIES"; offsetLatin += yylength(); }
	{Lat19}		{ latin = "NOVIESDECIES"; offsetLatin += yylength(); }
	{Lat20}		{ latin = "VICIES"; offsetLatin += yylength(); }
	{Lat21}		{ latin = "UNVICIES"; offsetLatin += yylength(); }
	{Lat22}		{ latin = "DUOVICIES"; offsetLatin += yylength(); }
	{Lat23}		{ latin = "TERVICIES"; offsetLatin += yylength(); }
	{Lat24}		{ latin = "QUATERVICIES"; offsetLatin += yylength(); }
	{Lat25}		{ latin = "QUINQUIESVICIES"; offsetLatin += yylength(); }

	[^]			{
					offsetLatin++;
					
					if( offsetLatin >= lengthLatin ) {
						
						if(prevState == articleList) { addArticle(value, latin, textLatin); }
						if(prevState == commaList) { /*addComma(value, latin, textLatin);*/ addParagraph(value, latin, textLatin);  }
						if(prevState == paragraphList) { addParagraph(value, latin, textLatin); }
						if(prevState == letterListBracket || prevState == letterListNoBracket) { addLetter(value, latin, textLatin); }
						if(prevState == artAliasArt) { addLetter(value, latin, textLatin); }
						
						if( offsetLatin > lengthLatin ) { yypushback(1); }
							
						yybegin(prevState);
					}
				}
}


{NAD}{ART_COMMA_ORD}			{ offset = 0; length = yylength(); yypushback(length); yybegin(artCommaOrd); }


<artCommaOrd>	{

	({ARTICLE}{SE}?)?{DD}		{ addArticle(readNumber(), yytext()); offset += yylength(); }
	
	{ord1}({SE}?{COMMA})?		{ addParagraph("1", yytext()); offset += yylength(); }
	{ord2}({SE}?{COMMA})?		{ addParagraph("2", yytext()); offset += yylength(); }
	{ord3}({SE}?{COMMA})?		{ addParagraph("3", yytext()); offset += yylength(); }
	{ord4}({SE}?{COMMA})?		{ addParagraph("4", yytext()); offset += yylength(); }
	{ord5}({SE}?{COMMA})?		{ addParagraph("5", yytext()); offset += yylength(); }			
	{ord6}({SE}?{COMMA})?		{ addParagraph("6", yytext()); offset += yylength(); }
	{ord7}({SE}?{COMMA})?		{ addParagraph("7", yytext()); offset += yylength(); }
	{ord8}({SE}?{COMMA})?		{ addParagraph("8", yytext()); offset += yylength(); }
	{ord9}({SE}?{COMMA})?		{ addParagraph("9", yytext()); offset += yylength(); }
	{ord10}({SE}?{COMMA})?		{ addParagraph("10", yytext()); offset += yylength(); }	
	{ord11}({SE}?{COMMA})?		{ addParagraph("11", yytext()); offset += yylength(); }
	{ord12}({SE}?{COMMA})?		{ addParagraph("12", yytext()); offset += yylength(); }	
	{ord13}({SE}?{COMMA})?		{ addParagraph("13", yytext()); offset += yylength(); }
	{ord14}({SE}?{COMMA})?		{ addParagraph("14", yytext()); offset += yylength(); }	
	{ord15}({SE}?{COMMA})?		{ addParagraph("15", yytext()); offset += yylength(); }
	{ord16}({SE}?{COMMA})?		{ addParagraph("16", yytext()); offset += yylength(); }	
	{ord17}({SE}?{COMMA})?		{ addParagraph("17", yytext()); offset += yylength(); }
	{ord18}({SE}?{COMMA})?		{ addParagraph("18", yytext()); offset += yylength(); }	
	{ord19}({SE}?{COMMA})?		{ addParagraph("19", yytext()); offset += yylength(); }
	{ord20}({SE}?{COMMA})?		{ addParagraph("20", yytext()); offset += yylength(); }	
	{ord21}({SE}?{COMMA})?		{ addParagraph("21", yytext()); offset += yylength(); }
	{ord22}({SE}?{COMMA})?		{ addParagraph("22", yytext()); offset += yylength(); }	
	{ord23}({SE}?{COMMA})?		{ addParagraph("23", yytext()); offset += yylength(); }
	{ord24}({SE}?{COMMA})?		{ addParagraph("24", yytext()); offset += yylength(); }	
	{ord25}({SE}?{COMMA})?		{ addParagraph("25", yytext()); offset += yylength(); }	
	
	{ordLast}({SE}?{COMMA})?	{ addStopword(yytext()); offset += yylength(); }	
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
				
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}



{NAD}{ART_ALIAS_ART}			{ offset = 0; length = yylength(); yypushback(length); yybegin(artAliasArt); }


<artAliasArt>	{

	({ARTICLE}{SE}?)?{DD}		{ addArticle(readNumber(), yytext()); offset += yylength(); }
	
	{ord1}({SE}?{COMMA})?		{ addParagraph("1", yytext()); offset += yylength(); }
	{ord2}({SE}?{COMMA})?		{ addParagraph("2", yytext()); offset += yylength(); }
	{ord3}({SE}?{COMMA})?		{ addParagraph("3", yytext()); offset += yylength(); }
	{ord4}({SE}?{COMMA})?		{ addParagraph("4", yytext()); offset += yylength(); }
	{ord5}({SE}?{COMMA})?		{ addParagraph("5", yytext()); offset += yylength(); }			
	{ord6}({SE}?{COMMA})?		{ addParagraph("6", yytext()); offset += yylength(); }
	{ord7}({SE}?{COMMA})?		{ addParagraph("7", yytext()); offset += yylength(); }
	{ord8}({SE}?{COMMA})?		{ addParagraph("8", yytext()); offset += yylength(); }
	{ord9}({SE}?{COMMA})?		{ addParagraph("9", yytext()); offset += yylength(); }
	{ord10}({SE}?{COMMA})?		{ addParagraph("10", yytext()); offset += yylength(); }	
	{ord11}({SE}?{COMMA})?		{ addParagraph("11", yytext()); offset += yylength(); }
	{ord12}({SE}?{COMMA})?		{ addParagraph("12", yytext()); offset += yylength(); }	
	{ord13}({SE}?{COMMA})?		{ addParagraph("13", yytext()); offset += yylength(); }
	{ord14}({SE}?{COMMA})?		{ addParagraph("14", yytext()); offset += yylength(); }	
	{ord15}({SE}?{COMMA})?		{ addParagraph("15", yytext()); offset += yylength(); }
	{ord16}({SE}?{COMMA})?		{ addParagraph("16", yytext()); offset += yylength(); }	
	{ord17}({SE}?{COMMA})?		{ addParagraph("17", yytext()); offset += yylength(); }
	{ord18}({SE}?{COMMA})?		{ addParagraph("18", yytext()); offset += yylength(); }	
	{ord19}({SE}?{COMMA})?		{ addParagraph("19", yytext()); offset += yylength(); }
	{ord20}({SE}?{COMMA})?		{ addParagraph("20", yytext()); offset += yylength(); }	
	{ord21}({SE}?{COMMA})?		{ addParagraph("21", yytext()); offset += yylength(); }
	{ord22}({SE}?{COMMA})?		{ addParagraph("22", yytext()); offset += yylength(); }	
	{ord23}({SE}?{COMMA})?		{ addParagraph("23", yytext()); offset += yylength(); }
	{ord24}({SE}?{COMMA})?		{ addParagraph("24", yytext()); offset += yylength(); }	
	{ord25}({SE}?{COMMA})?		{ addParagraph("25", yytext()); offset += yylength(); }

	{ordLast}({SE}?{COMMA})?	{ addStopword(yytext()); offset += yylength(); }

	{LETTER}{SE}?{LettLatinBracket} { startLatin(); }


	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
				
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}


{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{
						addText(yytext());
					}


