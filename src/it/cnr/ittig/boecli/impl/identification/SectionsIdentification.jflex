/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CaseLawAuthority;
import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;


%%
%class SectionsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in


%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Court sections identification"; }

	@Override
	public String version() { return "0.4"; }
		

	/* An empty default constructor is required to comply with BOECLIService */
	
	public SectionsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private int offset = 0;
	private int length = 0;
	private String sectionText = "";
	private String value = "";

	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void saveSection(String value) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addSection(value, text.substring(1, text.length()-1));
		
		yypushback(1);
	}
	
	
	private void saveRegion() {
	
		String text = yytext();
		
		//[BOECLI:GEO:IT_...]
		
		value = text.substring(12,18);
	}
	
	private void saveCity() {
	
		String text = yytext();
		
		//[BOECLI:GEO:IT_..]
		
		value = text.substring(12,17);
	}
	
%} 

/* Section prefix */

SECTION = (sezion.)|(sez\.?)|(ss\.?)


/* Supreme Court sections */

LAV = (lavoro)|(lav\.?)|(l\.?)
TRIB = (tributaria)|(tribut\.?)|(trib\.?)|(t\.?)
FER = (feriale)|(fer\.?)|(f\.?)
UN = (unite)|(un\.?)|(u\.?)|(uu\.?)

SECTION_LAV = {SECTION}{SE}?{LAV}
SECTION_TRIB = {SECTION}{SE}?{TRIB}
SECTION_FER = {SECTION}{SE}?{FER}
SECTION_UN = {SECTION}{SE}?{UN}


/* Council of State sections */
NORM = (normativa)|(norm\.?)
SECTION_NORM = {SECTION}{SE}?{NORM}

ADUNANZA = (adunanza)|(ad\.)
PLENARIA = (plenaria)|(plen\.)
GENERALE = (generale)|(gen\.)
AD_PLENARIA = {ADUNANZA}{SE}?{PLENARIA}
AD_GENERALE = {ADUNANZA}{SE}?{GENERALE}


/* Number and Ordinal number sections (1-6 for Supreme Court and Council of State) */

FIRST = {Ord1}|(i)|(1[\^°ºªa])
SECOND = {Ord2}|(ii)|(2[\^°ºªa])
THIRD = {Ord3}|(iii)|(3[\^°ºªa])
FOURTH = {Ord4}|(iv)|(4[\^°ºªa])
FIFTH = {Ord5}|(v)|(5[\^°ºªa])
SIXTH = {Ord6}|(vi)|(6[\^°ºªa])

SEC_FIRST = ({SECTION}{SE}?{FIRST})|({FIRST}{SE}?{SECTION})|({SECTION}{SE}?(1))
SEC_SECOND = ({SECTION}{SE}?{SECOND})|({SECOND}{SE}?{SECTION})|({SECTION}{SE}?(2))
SEC_THIRD = ({SECTION}{SE}?{THIRD})|({THIRD}{SE}?{SECTION})|({SECTION}{SE}?(3))
SEC_FOURTH = ({SECTION}{SE}?{FOURTH})|({FOURTH}{SE}?{SECTION})|({SECTION}{SE}?(4))
SEC_FIFTH = ({SECTION}{SE}?{FIFTH})|({FIFTH}{SE}?{SECTION})|({SECTION}{SE}?(5))
SEC_SIXTH = ({SECTION}{SE}?{SIXTH})|({SIXTH}{SE}?{SECTION})|({SECTION}{SE}?(6))


/* Grand Chamber */

GRAND = (grande)|(gr\.)
CHAMBER = (camera)|(cam\.)
GC = [\[]?[G]\.?[C]\.?[\]]?
GRAND_CHAMBER = ({GRAND}{SE}?{CHAMBER})|{GC}


/* Detached sections */

City =  {BOPEN}GEO:IT_[A-Z][A-Z]\]{BE_BODY}
Region = {BOPEN}GEO:IT_[A-Z][A-Z][A-Z]\]{BE_BODY}
Location = {City}|{Region}


DETACHED = (distaccat.)|(staccat.)
DET_SECTION = {SECTION}({SE}?{DETACHED})?({SE}?{OF})?{SE}?{Location}




%x BE location   

%%

{NAD}{SECTION_LAV}{NAD}				{ saveSection("L"); }
{NAD}{SECTION_TRIB}{NAD}			{ saveSection("T"); }
{NAD}{SECTION_FER}{NAD}				{ saveSection("F"); }
{NAD}{SECTION_UN}{NAD}				{ saveSection("U"); }

{NAD}{SECTION_NORM}{NAD}			{ saveSection("N"); }
{NAD}{AD_PLENARIA}{NAD}				{ saveSection("AP"); }
{NAD}{AD_GENERALE}{NAD}				{ saveSection("AG"); }

{NAD}{SEC_FIRST}{NAD}				{ saveSection("1"); }
{NAD}{SEC_SECOND}{NAD}				{ saveSection("2"); }
{NAD}{SEC_THIRD}{NAD}				{ saveSection("3"); }
{NAD}{SEC_FOURTH}{NAD}				{ saveSection("4"); }
{NAD}{SEC_FIFTH}{NAD}				{ saveSection("5"); }
{NAD}{SEC_SIXTH}{NAD}				{ saveSection("6"); }


{NAD}{GRAND_CHAMBER}{NAD}			{ saveSection("GC"); }


{DET_SECTION}						{ 
										offset = 0; sectionText = ""; sectionText = yytext(); value = ""; length = yylength(); 
										yypushback(length); yybegin(location); 
									}

<location> {

	{City}		{ saveCity(); offset += yylength(); }
	
	{Region}	{ saveRegion(); offset += yylength(); }
		
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
				
				addSection(value, sectionText);
				
				yybegin(YYINITIAL);
			}
		}	
}


{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]    				{ 
						addText(yytext());
					}


