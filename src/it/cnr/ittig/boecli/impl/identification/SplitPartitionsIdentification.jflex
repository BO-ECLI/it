/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class SplitPartitionsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in



%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Split Partitions identification"; }

	@Override
	public String version() { return "0.2"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public SplitPartitionsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private int offset = 0;
	private int length = 0;
	
	private String articleValue = "";
	private String paragraphValue = "";
	private String letterValue = "";
	private String itemValue = "";
	
	private void start() {
	
		offset = 0;
		length = yylength();
		articleValue = "";
		
		yypushback(length);
		
		yybegin(split);
		
	}
	
	private static String ITEM = "[BOECLI:ITEM:";
	private static String LETTER = "[BOECLI:LETTER:";
	private static String PARAGRAPH = "[BOECLI:PARAGRAPH:";
	private static String ARTICLE = "[BOECLI:ARTICLE:";
	
	private void readArticle(String text) {
		
		int articleStart = text.indexOf(ARTICLE);

		if(articleStart > -1) {
			
			articleValue = text.substring(articleStart + ARTICLE.length(), text.substring(articleStart).indexOf("]") + articleStart);
		}
		
		addMisc(yytext());
	}
	
	private void save(String text) {
	
		int itemStart = text.indexOf(ITEM);
		int letterStart = text.indexOf(LETTER);
		int paragraphStart = text.indexOf(PARAGRAPH);
		
		if(itemStart > -1) {
			
			itemValue = text.substring(itemStart + ITEM.length(), text.substring(itemStart).indexOf("]") + itemStart);
		}
		
		if(letterStart > -1) {
			
			letterValue = text.substring(letterStart + LETTER.length(), text.substring(letterStart).indexOf("]") + letterStart);
		}
		
		if(paragraphStart > -1) {
			
			paragraphValue = text.substring(paragraphStart + PARAGRAPH.length(), text.substring(paragraphStart).indexOf("]") + paragraphStart);
		}

		addPartition(articleValue, paragraphValue, letterValue, itemValue, yytext());		
		
		paragraphValue = "";
		letterValue = "";
		itemValue = "";
	}
	
%} 


/* ai sensi dell'art. 360 c.p.c., n. 5) */



ART_SPLIT_PAR_ITEM = {BE_ARTICLE}{BE_SEP}?{BE_LEGISLATIONALIAS}({BE_SEP}?{BE_PARAGRAPH}({BE_SEP}?{BE_ITEM})*)+

ART_SPLIT_PAR_LET = {BE_ARTICLE}{BE_SEP}?{BE_LEGISLATIONALIAS}({BE_SEP}?{BE_PARAGRAPH}({BE_SEP}?{BE_LETTER})*)+

ART_SPLIT_LET = {BE_ARTICLE}{BE_SEP}?{BE_LEGISLATIONALIAS}({BE_SEP}?{BE_LETTER})+

ART_SPLIT_ITEM = {BE_ARTICLE}{BE_SEP}?{BE_LEGISLATIONALIAS}({BE_SEP}?{BE_ITEM})+



%x BE split  

%%


{ART_SPLIT_PAR_ITEM}|{ART_SPLIT_PAR_LET}|{ART_SPLIT_LET}|{ART_SPLIT_ITEM}	{ start(); }


<split> {

	{BE_ARTICLE}							{ readArticle(yytext()); offset += yylength(); }
	
	{BE_SEP}								{ addText(yytext());  offset += yylength(); }
	
	{BE_LEGISLATIONALIAS}					{ addText(yytext());  offset += yylength(); }
	
	{BE_PARAGRAPH}({BE_SEP}?{BE_ITEM})?		{ save(yytext()); offset += yylength(); }
	
	{BE_PARAGRAPH}({BE_SEP}?{BE_LETTER})?	{ save(yytext()); offset += yylength(); }

	{BE_LETTER}			{ save(yytext()); offset += yylength(); }
	
	{BE_ITEM}			{ save(yytext()); offset += yylength(); }


	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
				
				yybegin(YYINITIAL);
			}
		}	
}



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{
						addText(yytext());
					}


