/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.impl.Config;

%%
%class StopwordsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{

	/* Custom java code */

	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Italian Stopwords identification"; }

	@Override
	public String version() { return "0.7"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public StopwordsIdentification() { }

	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	private void save() {
		
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addStopword(text.substring(1, text.length()-1));
		
		yypushback(1);	
	}

	private void save(String value) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addStopword(value, text.substring(1, text.length()-1));
		
		yypushback(1);	
	}
	
	private void saveVersus() {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addVersus(text.substring(1, text.length()-1));
		
		yypushback(1);	
	}
	
	
%} 


VERSUS = (contro)|([c]\.?)|(c[\/])|(c[\\])|(v\.?)|(vs\.?)|(versus)

GENERIC = {DET}|{INDET}|{PREPOSITION}

PREFIX_CASE = (caus[ae])|(cas[oi])|(process[oi])|(procediment[oi])|(proc\.)|(ricors[oi])|(ric\.?)|(ricorrent[ei])|(appl\.)|(r\.?c\.?)

PREFIX_ISSUED = {ISSUED}{SE}{BY}

IDEM = (idem)|(id\.?)

SAME = (stess.)|(medesim.)|(quest.)

%x BE   

%%

{NAD}{AND}{NADD}				{ save("AND"); }

{NAD}{OR}{NADD}					{ save("OR"); }

{NAD}{OF}{NADD}					{ save("OF"); }

{NAD}{GENERIC}{NADD}			{ save(); }

{NAD}{PARTICLES}{NADD}			{ save(); }

{NAD}{PREFIX_ISSUED}{NAD}		{ save(); }

{NAD}{PREFIX_CASE}{NAD}			{ save(); }

{NAD}{IDEM}{NAD}				{ save(); }

{NAD}{SAME}{NAD}				{ save(); }

{NAD}{VERSUS}{NAD}				{ saveVersus(); }



{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						addText(yytext()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{ addText(yytext()); }


