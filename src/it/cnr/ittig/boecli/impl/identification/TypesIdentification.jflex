/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CommonCaseLawTypes;
import eu.boecli.common.CommonLegalTypes;
import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.common.Type;
import eu.boecli.service.identification.EntityIdentificationService;
import it.cnr.ittig.boecli.NationalLegislationAuthorities;
import it.cnr.ittig.boecli.NationalLegislationTypes;
import it.cnr.ittig.boecli.NationalCaseLawAuthorities;
import it.cnr.ittig.boecli.impl.Config;

%%
%class TypesIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in
%include ../NationalMacros.in

%{
	
	/* Custom java code */

    @Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "Types of document"; }

	@Override
	public String version() { return "1.4"; }


	/* An empty default constructor is required to comply with BOECLIService */
	
	public TypesIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private int offset = 0;
	private int length = 0;
	
	/* Use the auxiliary methods provided by the EntityIdentificationService to save the identified entities. */

	private void save(Type type) {
	
		String text = yytext();
		
		addText(text.substring(0,1));
		
		addType(type, text.substring(1, text.length()-1));
		
		yypushback(1);
	}

%} 

/* Identification of legal types of documents in the Italian language */ 


/* Abbreviations like "l." and "d." should be dealt in later stages */

JUDGMENT = (sentenza)|(sentenze)|(sent\.?)|(decisione)|(pronuncia)
CONSTITUTIONAL = (costituzionale)|(cost\.?)
JUDGMENT_COST = {JUDGMENT}{SE}?{CONSTITUTIONAL}

ORDER = (ordinanza)|(ordinanze)|(ord\.?)|(ordin\.?)
ORDER_COST = {ORDER}{SE}?{CONSTITUTIONAL}

DECREE_EXT = (decreto)|(decreti) /* Ambiguity: "dec." could be decision or decree */
DECREE = {DECREE_EXT}|(dec\.?)|(d\.?)

OPINION = (parere)


LAW_EXT = (legge)|(leggi)
LAW = {LAW_EXT}|(l\.?)

LAW_DELEGA = {LAW}{SE}?{DH}{SE}?(delega)

DECREE_LAW = {DECREE}{SDHE}?{LAW}

LGS = (legislativo)|(leg\.(vo)?)|(l\.?g\.?s\.?(vo)?)
LEG_DECREE = {DECREE}{SCDHE}?{LGS}

REGULATION = (regolamento)|(reg\.?)
CIRCULAR = (circolare)|(circ\.?)
MIN = (ministerial.)|(minist\.?)|(min\.?)|(m\.?)
DEC_MIN = {DECREE}{SDHE}?{MIN}
ORD_MIN = {ORDER}{SDHE}?{MIN}
REG_MIN = {REGULATION}{SDHE}?{MIN}
CIRC_MIN = {CIRCULAR}{SDHE}?{MIN}


ROYAL = (regio)|(r\.?)
ROYAL_DECREE = {ROYAL}{SDHE}?{DECREE}
ROYAL_DECREE_LAW = {ROYAL}{SDHE}?{DECREE}{SDHE}?{LAW}
ROYAL_LEG_DECREE = {ROYAL}{SDHE}?{DECREE}{SDHE}?{LGS}



DPR = (d\.?p\.?r\.?)

DPCM = (d\.?p\.?c\.?m\.?)


/* European legislation types of document */

COMMON = (comunitari.)
SUFFIX = {COMMON}|{TREATY}
DIRECTIVE = (direttiv[ae])({SE}?{SUFFIX})?
REGULATION = (regolament[oi])({SE}?{SUFFIX})?
DECISION = (decision[ei])({SE}?{SUFFIX})?
RECOMMENDATION = (raccomandazion[ei])({SE}?{SUFFIX})?




%x BE dpr dpcm cost  

%%


{NAD}{LAW_EXT}{NAD}					{ save(CommonLegislationTypes.LAW); }

{NAD}{LAW_DELEGA}{NAD}				{ save(CommonLegislationTypes.LAW); }

{NAD}{JUDGMENT}{NAD}				{ save(CommonCaseLawTypes.JUDGMENT); }

{NAD}{ORDER}{NAD}					{ save(CommonLegalTypes.ORDER); }
{NAD}{DECREE_EXT}{NAD}				{ save(CommonLegalTypes.DECREE); }

{NAD}{OPINION}{NAD}					{ save(CommonCaseLawTypes.OPINION); }


{NAD}{DECREE_LAW}{NAD}				{ save(CommonLegislationTypes.DECREE_LAW); }
{NAD}{LEG_DECREE}{NAD}				{ save(CommonLegislationTypes.LEGISLATIVE_DECREE); }

{NAD}{DEC_MIN}{NAD}					{ save(NationalLegislationTypes.IT_DEC_MIN); }
{NAD}{ORD_MIN}{NAD}					{ save(NationalLegislationTypes.IT_ORD_MIN); }
{NAD}{REG_MIN}{NAD}					{ save(NationalLegislationTypes.IT_REG_MIN); }
{NAD}{CIRC_MIN}{NAD}				{ save(NationalLegislationTypes.IT_CIRC_MIN); }

{NAD}{ROYAL_DECREE}{NAD}			{ save(CommonLegislationTypes.ROYAL_DECREE); }
{NAD}{ROYAL_DECREE_LAW}{NAD}		{ save(CommonLegislationTypes.ROYAL_DECREE_LAW); }
{NAD}{ROYAL_LEG_DECREE}{NAD}		{ save(CommonLegislationTypes.ROYAL_LEGISLATIVE_DECREE); }



{NAD}{DIRECTIVE}{NAD}				{ save(CommonLegislationTypes.DIRECTIVE); }
{NAD}{REGULATION}{NAD}				{ save(CommonLegislationTypes.REGULATION); }
{NAD}{DECISION}{NAD}				{ save(CommonLegislationTypes.DECISION); }
{NAD}{RECOMMENDATION}{NAD}			{ save(CommonLegislationTypes.RECOMMENDATION); }




{NAD}{DPR}{ND}	{ 
					addText(yytext().substring(0,1));
					offset = 1;
					length = yylength();
					yypushback(length-1);
					yybegin(dpr);
				}

<dpr> {
	
	(d\.?)			{ addType(CommonLegislationTypes.DECREE, yytext()); offset += yylength(); }
	
	(p\.?r\.?)		{ addAuthority(NationalLegislationAuthorities.IT_PRES_REP, yytext()); offset += yylength(); }

	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					yypushback(1);
					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}


{NAD}{DPCM}{ND}	{ 
					addText(yytext().substring(0,1));
					offset = 1;
					length = yylength();
					yypushback(length-1);
					yybegin(dpcm);
				}

<dpcm> {
	
	(d\.?)			{ addType(CommonLegislationTypes.DECREE, yytext()); offset += yylength(); }
	
	(p\.?c\.?m\.?)	{ addAuthority(NationalLegislationAuthorities.IT_PRES_CONS_MIN, yytext()); offset += yylength(); }

	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					yypushback(1);
					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}


({NAD}{JUDGMENT_COST}{ND})|
({NAD}{ORDER_COST}{ND})		{ 
								offset = 0;
								length = yylength();
								yypushback(length);
								yybegin(cost);
							}

<cost> {
	
	{JUDGMENT}			{ addType(CommonCaseLawTypes.JUDGMENT, yytext()); offset += yylength(); }
	
	{ORDER}				{ addType(CommonCaseLawTypes.ORDER, yytext()); offset += yylength(); }
	
	{CONSTITUTIONAL}	{ addAuthority(NationalCaseLawAuthorities.IT_COST, yytext()); offset += yylength(); }
	
	[^]		{
				offset++;
				
				if( offset >= length ) {
					
					yypushback(1);
					yybegin(YYINITIAL);
				}
				
				addText(yytext());
			}
}


{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}
			
<BE> {

	{BCLOS}			{ 
						addText(yytext());
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}


[^]    				{ addText(yytext()); }


