/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package it.cnr.ittig.boecli.impl.recognition;

import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.common.Subject;
import eu.boecli.engine.EngineDocument;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.impl.recognition.DefaultCaseLawReferenceRecognition;
import it.cnr.ittig.boecli.NationalCaseLawAuthorities;
import it.cnr.ittig.boecli.impl.Config;

public class NationalCaseLawReferenceRecognition extends DefaultCaseLawReferenceRecognition {

  	@Override
	public Language language() { return Config.LANG; }

	@Override
	public Jurisdiction jurisdiction() { return Config.JUR; }

	@Override
	public String author() { return Config.AUTHOR; }

	@Override
	public String extensionName() { return Config.NAME; }

	@Override
	public String serviceName() { return "National case-law reference recognition"; }

	@Override
	public String version() { return "0.1"; }

	@Override
	public boolean highPriority() {

		return true;
	}
	
	@Override
	protected final void addCustomFeatures(LegalReference legalReference) {
		
		String section = legalReference.getSection();
		String authority = legalReference.getAuthority();

		//Adds CASS as authority if input is issued by CASS and section pertains to CASS 
		if(authority.equals("")) {
			
			if( ((EngineDocument) getEngineDocument()).getInputAuthority().equalsIgnoreCase("cass") ) {
				
				if( !section.equals("")) {
				
					if( section.equals("1") || section.equals("2") || section.equals("3") || section.equals("4") || section.equals("5") || section.equals("6") || 
							section.equals("T") || section.equals("L") || section.equals("F") || section.equals("U") ) {
						
						legalReference.setAuthority(NationalCaseLawAuthorities.IT_CASS.toString());
						
						legalReference.setCaseLawReference(true);
					}
				}
			}
		}
		
		//Adds subject if section pertains to CASS
		if(legalReference.getSubject().equals("")) {
			
			if( ((EngineDocument) getEngineDocument()).getInputAuthority().equalsIgnoreCase("cass") ||
					authority.equals(NationalCaseLawAuthorities.IT_CASS.toString())) {
			
				if(section.equals("L") || section.equals("T")) {
					
					legalReference.setSubject(Subject.CIVIL.toString());
				}
				
				if(section.equals("F")) {
					
					legalReference.setSubject(Subject.CRIMINAL.toString());
				}
			}
		}
	
		//TODO if input is CASS/COST, type is Judgment and auth is empty, then auth is CASS/COST ?
		//Ambiguity with abbreviated versions of previous complete citations.
		
		
		//if authority is empty and section is "Normativa", then authority is Council of State
		if(authority.equals("") && section.equals("N")) {
			
			legalReference.setAuthority(NationalCaseLawAuthorities.IT_CDS.toString());
		}
		
		
		//TODO Remove applicant if the court is not right ?
		String applicant = legalReference.getApplicant();		
		String defendant = legalReference.getDefendant();
		
		if( !applicant.equals("") && defendant.equals("")) {
			
			if( !authority.equals(CommonCaseLawAuthorities.CJEU) && !authority.equals(CommonCaseLawAuthorities.ECHR) && 
					authority.equals(NationalCaseLawAuthorities.IT_CASS) ) {
			}
		}
	}

	@Override
	protected void addCommonFeatures(LegalReference legalReference) {

		//Enable/disable here the add of common features depending on specific situations
		
		super.addCommonFeatures(legalReference);
	}
}
